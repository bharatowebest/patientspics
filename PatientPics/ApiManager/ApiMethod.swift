//
//  ApiManager.swift
//  Evolet
//
//  Created by Raghu on 19/07/19.
//  Copyright © 2019 cloud. All rights reserved.
//

import Foundation
import Alamofire
import Alamofire_SwiftyJSON
import SwiftyJSON
import UIKit

func getRequest(strUrl : String , success: @escaping (JSON)->Void , failure:@escaping (_ err:String)-> Void){
    
    guard Reachability.isConnectedToNetwork() else{
        failure("Please check your internrt connection")
        return
    }
    
    
    let url = URL(string: BASE_URL+strUrl)!
    
    var header = [String:String]()
    
    let login = UserDefaults.standard.getLogin()
    
    if login{
        let token  = "Bearer" + UserDefaults.standard.string(forKey: "token")!
        header = ["Authorization":token]
    }
    else{
        header = [:]
    }
    
    Alamofire.request(url, method: .get, parameters: [:], headers: header).responseSwiftyJSON { (response) in
        if response.result.value != nil{
            switch response.result{
            case .success:
                success(response.result.value!)
            case .failure:
                failure("")
            }
        }
        else{
            failure("something went wrong".localizedLowercase)
        }
    }
}


func postRequest(strUrl : String , param :[String:Any] , success: @escaping (JSON)->Void , failure:@escaping (_ err:String)-> Void) {
    
    guard Reachability.isConnectedToNetwork() else{
        failure("Please check your internrt connection")
        return
    }
    
    let url = URL(string: BASE_URL+strUrl)!
    var header = [String:String]()
    let login = UserDefaults.standard.getLogin()
    if login{
        let token  = "Bearer " + UserDefaults.standard.string(forKey: "token")!
        header = ["Authorization":token]
    }
    else{
        header = [:]
    }
    
    
    Alamofire.request(url, method: .post, parameters: param, headers: header).responseSwiftyJSON { (response) in
        if response.result.value != nil{
            switch response.result{
            case .success:
                success(response.result.value!)
            case .failure:
                failure("")
            }
        }
        else{
            failure("something went wrong".localizedLowercase)
        }
        
    }
}


//func uploadData(strUrl : String ,arrImage : [[String:Any]], param :[String:Any] , success: @escaping (JSON)->Void , failure:@escaping (Error)-> Void) {
//    
//    guard Reachability.isConnectedToNetwork() else{
//        failure("Please check your internrt connection")
//        return
//    }
//    
//    let url = URL(string: BASE_URL+strUrl)!
//    
//    var header = [String:String]()
//    if let isLogin = UserDefaults.standard.value(forKey: "isLogin") as? Bool{
//        if isLogin{
//            //            let decoded  = UserDefaults.standard.data(forKey: "varificationdata")
//            //            let decoder = JSONDecoder()
//            //            let decodedData = try? decoder.decode(VarificationRootClass.self, from: decoded!)
//            //            let token = decodedData?.token
//            //            header = ["Authorization":"Bearer \(String(describing: token! ))"]
//        }
//        else{
//            header = [:]
//        }
//    }
//    else{
//        header = [:]
//    }
//    
//    
//    Alamofire.upload(multipartFormData: { multipartFormData in
//        // import image to request
//        for imageData in arrImage {
//            let img = imageData["img"] as! UIImage
//            let key = imageData["key"]
//            let data = img.jpegData(compressionQuality: 0.5)
//            multipartFormData.append(data!, withName: key as! String, fileName: "\(Date().timeIntervalSince1970).jpeg", mimeType: "image/jpeg")
//        }
//        for (key, value) in param {
//            multipartFormData.append("\(value)".data(using: String.Encoding.utf8)!, withName: key)
//        }
//    }, to: url,headers:header,
//       
//       encodingCompletion: { encodingResult in
//        switch encodingResult {
//        case .success(let upload, _, _):
//            upload.responseSwiftyJSON(completionHandler: { (response) in
//                success(response.result.value!)
//            })
//        case .failure(let error):
//            print(error)
//        }
//        
//    })
//}
