//
//  GlobalObjects.swift
//  FitnessApp
//
//  Created by Ankit Bagda on 26/03/19.
//  Copyright © 2019 Ritesh Jain. All rights reserved.
//

import Foundation
import UIKit

//GlobalObjectes
//local
let BASE_URL = "http://patientpics.dev2.obtech.inet/api/"
let imageUrl = "http://patientpics.dev2.obtech.inet/"

//live

let APP_DELEGATE = UIApplication.shared.delegate as! AppDelegate
let DEVICE_TYPE = "iphone"
let DEVICE_ID = UIDevice.current.identifierForVendor?.uuidString
let APP_THEME_COLOR = UIColor(displayP3Red: 0.196, green: 0.796, blue: 0.784, alpha: 1.0)

//let ROLE_ID =

func AppGlobalFont(_ fontSize:CGFloat,isBold:Bool) -> UIFont {
    let fontName : String!
    fontName = (isBold) ? "Roboto Medium" : "Roboto"
    return UIFont(name: fontName, size: fontSize)!
}

func JSONStringify(value: AnyObject,prettyPrinted:Bool = false) -> String
{
    
    let options = prettyPrinted ? JSONSerialization.WritingOptions.prettyPrinted : JSONSerialization.WritingOptions(rawValue: 0)
    if JSONSerialization.isValidJSONObject(value) {
        do{
            let data = try JSONSerialization.data(withJSONObject: value, options: options)
            if let string = NSString(data: data, encoding: String.Encoding.utf8.rawValue) {
                return string as String
            }
        }catch {
            print("error")
        }
    }
    return ""
}


struct MessageString{
    static  let INTERNET_CONNECTION_MESSAGE = "Please check your internrt connection"
    static let Login_Field_validation = "Please fill mobile/email field"
}
