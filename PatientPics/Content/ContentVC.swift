//
//  ContentVC.swift
//  PatientPics
//
//  Created by Ankit Bagda on 17/08/19.
//  Copyright © 2019 Om Prakash Jangid. All rights reserved.
//

import UIKit

class ContentVC: UIViewController {

    @IBOutlet weak var addBinderBtn: UIButton!
    @IBOutlet weak var titleTextField: UITextField!
    @IBOutlet weak var userImgView: UIImageView!
    @IBOutlet weak var imgEditBtn: UIButton!
    @IBOutlet weak var viewContent: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        userImgView.layer.cornerRadius = userImgView.frame.height / 2
        imgEditBtn.layer.cornerRadius = imgEditBtn.frame.height / 2
        addBinderBtn.layer.cornerRadius = addBinderBtn.frame.height / 2
        viewContent.layer.borderWidth = 0.25
        viewContent.layer.borderColor = UIColor.black.cgColor
    }
    override func viewWillAppear(_ animated: Bool) {
        
     self.title = "PatientPics"
        
        super.viewWillAppear(true)
        self.navigationItem.leftBarButtonItem = nil
        self.navigationController?.navigationBar.isHidden = false
        let backBarButtonItem = UIBarButtonItem(image: UIImage(named:"menu-button-2.png"), style: .plain, target: self, action: #selector(backBtn))
        self.navigationItem.leftBarButtonItem  = backBarButtonItem
        navigationController?.navigationBar.tintColor = .white
        
        var tabview = UIView()
        tabview = UIView(frame: CGRect(x: 0, y: 0, width: 60, height: 40))
       
        let notificationBarButtonItem = UIButton(frame: CGRect(x: 40, y: 0, width: 36, height: 36))
        notificationBarButtonItem.setImage(UIImage(named: "bell20.png"), for: .normal)
        notificationBarButtonItem.addTarget(self, action: #selector(self.notificationBarButtonItemAction), for: UIControl.Event.touchUpInside)
        
        var notificationLabel = UILabel()
        notificationLabel = UILabel(frame: CGRect(x: 57, y: 2, width: 16, height: 16))
        notificationLabel.backgroundColor = UIColor.red
        notificationLabel.layer.cornerRadius = notificationLabel.frame.height / 2
        notificationLabel.layer.masksToBounds = true
        notificationLabel.text = "3"
        notificationLabel.textAlignment = .center
        notificationLabel.font = UIFont.boldSystemFont(ofSize: 16)
        notificationLabel.textColor = UIColor.white
        tabview.addSubview(notificationBarButtonItem)
        tabview.addSubview(notificationLabel)
        
        
        let rightBarButton = UIBarButtonItem(customView: tabview)
        self.navigationItem.rightBarButtonItem = rightBarButton
    }
    
    @objc func addBarButtonItemAction()
    {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "SettingAddProviderVC") as! SettingAddProviderVC
        vc.view.frame = self.view.bounds
        self.addChild(vc)
        self.view.addSubview(vc.view)
        vc.didMove(toParent: self)
    }
    
    @objc func notificationBarButtonItemAction()
    {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "NotificationListVC") as! NotificationListVC
        navigationController?.pushViewController(vc, animated: true)
    }
    
    @objc func backBtn()
    {
        sideMenuController?.showLeftViewAnimated()
    }

    @IBAction func userimgEditBtnAction(_ sender: Any)
    {
        
    }
    
    @IBAction func addBinderAction(_ sender: Any)
    {
        
    }
}
