//
//  DashboardVC.swift
//  PatientPics
//
//  Created by Ankit Bagda on 05/08/19.
//  Copyright © 2019 Om Prakash Jangid. All rights reserved.
//

import UIKit
import LGSideMenuController

class DashboardVC: UIViewController,UITableViewDelegate, UITableViewDataSource, UIPickerViewDelegate,UIPickerViewDataSource
{

    @IBOutlet weak var tblView: UITableView!
    @IBOutlet weak var picker: UIPickerView!
    var arrPatients = ["John Doe", "Harry Lobbert", "Tom Here", "Abel Adem", "Camille Abraham"]
    var arrDoctors = ["John Doe", "Harry Lobbert", "Tom Here", "Abel Adem", "Camille Abraham"]
    var arrColor = [UIColor.orange, UIColor.red, UIColor.cyan, UIColor.purple, UIColor.orange]
    let arrMonth = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"]
    let arrPatientOptions = ["Day", "Week"]
    let arrDoctorOptions = ["Month", "Year"]
    var showVal = 0
    var btnTitleTotal = "  Period: June       v  "
    var btnTitlePatients = " Period: Day      v "
    var btnTitleDoctor = " Period: Day      v "
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        picker.delegate = self
        picker.dataSource = self
        picker.isHidden = true
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.navigationItem.leftBarButtonItem = nil
        self.navigationController?.navigationBar.isHidden = false
        self.title = "Dashboard"
        let backBarButtonItem = UIBarButtonItem(image: UIImage(named: "menu-button-2.png"), style: .plain, target: self, action: #selector(menuBtn))
        self.navigationItem.leftBarButtonItem  = backBarButtonItem
        
        var tabview = UIView()
        tabview = UIView(frame: CGRect(x: 0, y: 0, width: 40, height: 30))
        
        let notificationBarButtonItem = UIButton(frame: CGRect(x: 20, y: 0, width: 30, height: 30))
        notificationBarButtonItem.setImage(UIImage(named: "bell20.png"), for: .normal)
        notificationBarButtonItem.addTarget(self, action: #selector(self.notificationBarButtonItemAction), for: UIControl.Event.touchUpInside)
        
       
        navigationController?.navigationBar.tintColor = .white
        var notificationLabel = UILabel()
        notificationLabel = UILabel(frame: CGRect(x: 37, y: 1, width: 16, height: 16))
        notificationLabel.backgroundColor = UIColor.red
        notificationLabel.layer.cornerRadius = notificationLabel.frame.height / 2
        notificationLabel.layer.masksToBounds = true
        notificationLabel.text = "3"
        notificationLabel.textAlignment = .center
        notificationLabel.font = UIFont.boldSystemFont(ofSize: 16)
        notificationLabel.textColor = UIColor.white
        navigationController?.navigationBar.tintColor = .white
        
        tabview.addSubview(notificationBarButtonItem)
         tabview.addSubview(notificationLabel)
        
        let rightBarButton = UIBarButtonItem(customView: tabview)
        self.navigationItem.rightBarButtonItem = rightBarButton
    }
    
    @objc func menuBtn()
    {
        sideMenuController!.leftViewWidth = 250.0
        sideMenuController?.showLeftViewAnimated()
    }
    
    @objc func  notificationBarButtonItemAction()
    {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "NotificationListVC") as! NotificationListVC
        navigationController?.pushViewController(vc, animated: true)
    }
    
    //MARK: - TableView Delegates and Datasources
    func numberOfSections(in tableView: UITableView) -> Int
    {
        return 3
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView?
    {
        print(section)
        let headerFrame = tableView.frame
        let headerView:UIView = UIView(frame: CGRect(x: 0, y: 0, width: headerFrame.size.width, height: headerFrame.size.height))
        
        headerView.backgroundColor = UIColor.white
        if section == 0
        {
            let lblHeading = UILabel()
            lblHeading.frame = CGRect(x: 8, y: 0, width: 250, height: 40)
            lblHeading.text = "Welcome to your Dashboard!"
            headerView.addSubview(lblHeading)
            return headerView
        }
        else if section == 1
        {
            let lblHeading = UILabel()
            lblHeading.frame = CGRect(x: 8, y: 16, width: 150, height: 30)
            lblHeading.text = "Recent Patients"
            lblHeading.textColor = UIColor(red: 0/255, green: 188/255, blue: 184/255, alpha: 1.0)
            lblHeading.font = UIFont.boldSystemFont(ofSize: 20.0)
            let btnPeriod = UIButton()
            btnPeriod.frame = CGRect(x: headerView.frame.width - 134, y: 16, width: 130, height: 30)
            btnPeriod.setTitleColor(UIColor.black, for: .normal)
            btnPeriod.addTarget(self, action: #selector(showPickerPatient), for: .touchUpInside)
            btnPeriod.setTitle(btnTitlePatients, for: .normal)
            btnPeriod.layer.borderWidth = 1
            btnPeriod.layer.borderColor = UIColor.black.cgColor
            btnPeriod.layer.cornerRadius = btnPeriod.frame.height / 2
            btnPeriod.titleLabel?.font = UIFont(name: "Helvetica neue", size: 14.0)
            headerView.addSubview(lblHeading)
            headerView.addSubview(btnPeriod)
            return headerView
        }
        else
        {
            let lblHeading = UILabel()
            lblHeading.frame = CGRect(x: 8, y: 16, width: 150, height: 30)
            lblHeading.text = "Recent Doctors"
            lblHeading.font = UIFont.boldSystemFont(ofSize: 20.0)
            lblHeading.textColor = UIColor(red: 0/255, green: 188/255, blue: 184/255, alpha: 1.0)
            let btnPeriod = UIButton()
            btnPeriod.frame = CGRect(x: headerView.frame.width
                - 134, y: 16, width: 130, height: 30)
            btnPeriod.setTitleColor(UIColor.black, for: .normal)
            btnPeriod.addTarget(self, action: #selector(showPickerDoctor), for: .touchUpInside)
            btnPeriod.setTitle(btnTitleDoctor, for: .normal)
            btnPeriod.layer.borderWidth = 1
            btnPeriod.layer.borderColor = UIColor.black.cgColor
            btnPeriod.layer.cornerRadius = btnPeriod.frame.height / 2
            btnPeriod.titleLabel?.font = UIFont(name: "Helvetica neue", size: 14.0)
            headerView.addSubview(lblHeading)
            headerView.addSubview(btnPeriod)
            return headerView
        }
        
        
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat
    {
        if section == 0
        {
            return 50
        }
        else if section == 1
        {
            return 50
        }
        else
        {
            return 50
        }
        
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        if section == 0
        {
            return 1
        }
        else if section == 1
        {
            return arrPatients.count
        }
        else
        {
            return arrDoctors.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        //MARK: - Cell Total Patients
        if indexPath.section == 0
        {
            let cellTotal = tblView.dequeueReusableCell(withIdentifier: "TotalPatientsCell") as! TotalPatientsCell
            cellTotal.btnPosted.setTitle(btnTitleTotal, for: .normal)
            cellTotal.btnPosted.addTarget(self, action: #selector(showPickerTotal), for: .touchUpInside)
            return cellTotal
        }
            //MARK: - Cell Patients
        else if indexPath.section == 1
        {
            let cellPatients = tblView.dequeueReusableCell(withIdentifier: "RecentPatientsCell") as! RecentPatientsCell
            cellPatients.lblNameInitial.layer.cornerRadius = 17
            cellPatients.lblNameInitial.clipsToBounds = true
            cellPatients.lblNameInitial.backgroundColor = arrColor[indexPath.row]
            cellPatients.lblNameInitial.text = "\(arrPatients[indexPath.row].prefix(1))"
            cellPatients.lblName.text = arrPatients[indexPath.row]
            return cellPatients
        }
            //MARK: - Cell Doctor
        else
        {
            let cellDoctor = tblView.dequeueReusableCell(withIdentifier: "RecentDoctorsCell") as! RecentDoctorsCell
            cellDoctor.lblNameInitial.layer.cornerRadius = 17
            cellDoctor.lblNameInitial.clipsToBounds = true
            cellDoctor.lblNameInitial.backgroundColor = arrColor[indexPath.row]
            cellDoctor.lblName.text = arrPatients[indexPath.row]
            cellDoctor.lblNameInitial.text = "\(arrPatients[indexPath.row].prefix(1))"
            return cellDoctor
        }
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        if indexPath.section == 0
        {
            return 200
        }
        else if indexPath.section == 1
        {
            return 60
        }
        else
        {
            return 60
        }
        
    }
    
    @objc func showPickerTotal()
    {
        picker.delegate = self
        picker.dataSource = self
        picker.isHidden = false
        showVal = 1
    }
    
    @objc func showPickerPatient()
    {
        picker.delegate = self
        picker.dataSource = self
        picker.isHidden = false
        showVal = 2
    }
    
    @objc func showPickerDoctor()
    {
        picker.delegate = self
        picker.dataSource = self
        picker.isHidden = false
        showVal = 3
    }
    
    @IBAction func Menu(_ sender: Any)
    {
        sideMenuController?.showLeftViewAnimated()
    }
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int
    {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int
    {
        if showVal == 1
        {
            return arrMonth.count
        }
        else if showVal == 2
        {
            return arrPatientOptions.count
        }
        else
        {
            return arrDoctorOptions.count
        }
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String?
    {
        if showVal == 1
        {
            return arrMonth[row]
        }
        else if showVal == 2
        {
            return arrPatientOptions[row]
        }
        else
        {
            return arrDoctorOptions[row]
        }
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int)
    {
        if showVal == 1
        {
            btnTitleTotal = "  Period: " + "\(arrMonth[row])" + "      v  "
            picker.isHidden = true
            tblView.reloadData()
        }
        else if showVal == 2
        {
            btnTitlePatients = "  Period: " + "\(arrPatientOptions[row])" + "      v  "
            picker.isHidden = true
            tblView.reloadData()
        }
        else
        {
            btnTitleDoctor = "  Period: " + "\(arrDoctorOptions[row])" + "      v  "
            picker.isHidden = true
            tblView.reloadData()
        }
        
    }
    
}
