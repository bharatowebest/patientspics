//
//  DocumentVC.swift
//  PatientPics
//
//  Created by Ankit Bagda on 09/08/19.
//  Copyright © 2019 Om Prakash Jangid. All rights reserved.
//

import UIKit

class DocumentVC: UIViewController,UITableViewDataSource,UITableViewDelegate {
   
    @IBOutlet weak var tableView: UITableView!
    

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.navigationItem.leftBarButtonItem = nil
        self.navigationController?.navigationBar.isHidden = false
        self.title = "Documents"
        let backBarButtonItem = UIBarButtonItem(image: UIImage(named: "menu-button-2.png"), style: .plain, target: self, action: #selector(backBtn))
        self.navigationItem.leftBarButtonItem  = backBarButtonItem
        
        var tabview = UIView()
        tabview = UIView(frame: CGRect(x: 0, y: 0, width: 80, height: 30))
        
        let notificationBarButtonItem = UIButton(frame: CGRect(x: 60, y: 0, width: 30, height: 30))
        notificationBarButtonItem.setImage(UIImage(named: "bell20.png"), for: .normal)
        notificationBarButtonItem.addTarget(self, action: #selector(self.notificationBarButtonItemAction), for: UIControl.Event.touchUpInside)
        
        let addBarButtonItem = UIButton(frame: CGRect(x: 30, y: 0, width: 30, height: 30))
        addBarButtonItem.setImage(UIImage(named: "add20w.png"), for: .normal)
        addBarButtonItem.addTarget(self, action: #selector(self.addBarButtonItemAction), for: UIControl.Event.touchUpInside)
        
        let searchBarButtonItem = UIButton(frame: CGRect(x: 0, y: 0, width: 30, height: 30))
        searchBarButtonItem.setImage(UIImage(named: "search20.png"), for: .normal)
        searchBarButtonItem.addTarget(self, action: #selector(self.searchBarButtonItemAction), for: UIControl.Event.touchUpInside)
        navigationController?.navigationBar.tintColor = .white
        
        var notificationLabel = UILabel()
        notificationLabel = UILabel(frame: CGRect(x: 77, y: 2, width: 16, height: 16))
        notificationLabel.backgroundColor = UIColor.red
        notificationLabel.layer.cornerRadius = notificationLabel.frame.height / 2
        notificationLabel.layer.masksToBounds = true
        notificationLabel.text = "3"
        notificationLabel.textAlignment = .center
        notificationLabel.font = UIFont.boldSystemFont(ofSize: 16)
        notificationLabel.textColor = UIColor.white
        
        tabview.addSubview(notificationBarButtonItem)
        tabview.addSubview(addBarButtonItem)
        tabview.addSubview(searchBarButtonItem)
        tabview.addSubview(notificationLabel)
        
        let rightBarButton = UIBarButtonItem(customView: tabview)
        self.navigationItem.rightBarButtonItem = rightBarButton
    }
    
    @objc func backBtn()
    {
        sideMenuController?.showLeftViewAnimated()
    }
    
    @objc func  notificationBarButtonItemAction()
    {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "NotificationListVC") as! NotificationListVC
        navigationController?.pushViewController(vc, animated: true)
    }
    
    @objc func  addBarButtonItemAction()
    {
        
    }
    
    @objc func  searchBarButtonItemAction()
    {
        
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 6
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "DocumentListCell", for: indexPath as IndexPath) as! DocumentListCell
        cell.layer.masksToBounds = true
        cell.layer.borderWidth = 0.5
        cell.layer.borderColor = UIColor.lightGray.cgColor
        tableView.tableFooterView = UIView()
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        let secondViewController = self.storyboard?.instantiateViewController(withIdentifier: "rootViewController") as! rootViewController
        self.navigationController?.pushViewController(secondViewController, animated: true)
    }

}
