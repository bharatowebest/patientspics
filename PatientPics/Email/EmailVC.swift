//
//  EmailVC.swift
//  PatientPics
//
//  Created by Ankit Bagda on 03/08/19.
//  Copyright © 2019 Om Prakash Jangid. All rights reserved.
//

import UIKit

class EmailVC: UIViewController {

    @IBOutlet weak var emailBtn: UIButton!
    @IBOutlet weak var cancelBtn: UIButton!
    @IBOutlet weak var messageTextView: UITextView!
    @IBOutlet weak var subjectTextField: UITextField!
    @IBOutlet weak var EmailAddressEnterTextField: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        cancelBtn.layer.cornerRadius = cancelBtn.frame.height / 2
        cancelBtn.layer.borderWidth = 1
        cancelBtn.layer.borderColor = (config.appThemeColor.cgColor)
        cancelBtn.titleLabel!.textColor = config.appThemeColor
        
        emailBtn.layer.cornerRadius = emailBtn.frame.height / 2
        emailBtn.layer.borderWidth = 1
        emailBtn.layer.borderColor = config.appThemeColor.cgColor
        emailBtn.titleLabel?.textColor = UIColor.white
        emailBtn.titleLabel!.textColor = UIColor.white
        emailBtn.backgroundColor = config.appThemeColor
        
        messageTextView.layer.borderWidth = 1
        messageTextView.layer.borderColor = #colorLiteral(red: 0.7326697335, green: 0.7326697335, blue: 0.7326697335, alpha: 1)
    }
    

    @IBAction func cancelAction(_ sender: Any) {
        cancelBtn.setTitleColor(#colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0), for: .normal)
        cancelBtn.backgroundColor = config.appThemeColor
        emailBtn.titleLabel?.textColor = config.appThemeColor
        emailBtn.backgroundColor = UIColor.white
    }
    
    @IBAction func emailAction(_ sender: Any) {
        emailBtn.setTitleColor(#colorLiteral(red: 1, green: 1, blue: 1, alpha: 1), for: .normal)
        emailBtn.backgroundColor = config.appThemeColor
        cancelBtn.setTitleColor(config.appThemeColor, for: .normal)
        cancelBtn.backgroundColor = UIColor.white
    }
    
}
