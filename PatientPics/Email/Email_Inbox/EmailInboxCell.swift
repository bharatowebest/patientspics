//
//  EmailInboxCell.swift
//  PatientPics
//
//  Created by Ankit Bagda on 03/08/19.
//  Copyright © 2019 Om Prakash Jangid. All rights reserved.
//

import UIKit

class EmailInboxCell: UITableViewCell {

    @IBOutlet weak var messageDescriptionLbl: UILabel!
    @IBOutlet weak var appointmentTimeLbl: UILabel!
    @IBOutlet weak var nameLbl: UILabel!
    @IBOutlet weak var checkBoxBtn: UIButton!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
