//
//  EmailInboxVC.swift
//  PatientPics
//
//  Created by Ankit Bagda on 03/08/19.
//  Copyright © 2019 Om Prakash Jangid. All rights reserved.
//

import UIKit

class EmailInboxVC: UIViewController, UITableViewDataSource,UITableViewDelegate {
    
    

    @IBOutlet weak var filterDownArrowBtn: UIButton!
    @IBOutlet weak var filterLbl: UILabel!
    @IBOutlet weak var editBtn: UIButton!
    @IBOutlet weak var menuBtn: UIButton!
    @IBOutlet weak var usernameLbl: UILabel!
    @IBOutlet weak var tableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        filterLbl.layer.cornerRadius = 12
        filterLbl.layer.borderWidth = 0.8
        filterLbl.layer.borderColor = #colorLiteral(red: 0.501960814, green: 0.501960814, blue: 0.501960814, alpha: 1)
        tableView.rowHeight = 70
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 20
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "EmailInboxCell", for: indexPath as IndexPath) as! EmailInboxCell
        return cell
    }

}
