//
//  RecentDoctorsCell.swift
//  PatientPics
//
//  Created by apple on 7/29/19.
//  Copyright © 2019 apple. All rights reserved.
//

import UIKit

class RecentDoctorsCell: UITableViewCell
{
    @IBOutlet weak var lblName : UILabel!
    @IBOutlet weak var lblNameInitial : UILabel!

    override func awakeFromNib()
    {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool)
    {
        super.setSelected(selected, animated: animated)
    }

}
