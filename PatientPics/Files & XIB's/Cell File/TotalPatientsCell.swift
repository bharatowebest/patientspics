//
//  TotalPatientsCell.swift
//  PatientPics
//
//  Created by apple on 7/29/19.
//  Copyright © 2019 apple. All rights reserved.
//

import UIKit

class TotalPatientsCell: UITableViewCell
{

    @IBOutlet weak var viewMain: UIView!
    @IBOutlet weak var lblTotal: UILabel!
    @IBOutlet weak var btnPosted: UIButton!
    
    override func awakeFromNib()
    {
        super.awakeFromNib()
        
        btnPosted.layer.borderColor = UIColor.black.cgColor
        btnPosted.layer.borderWidth = 1
        btnPosted.layer.cornerRadius = btnPosted.frame.height / 2
        //MARK:- Graph View
        
        let myData =
            [
                ["1": 15],
                ["2" : 30],
                ["3" : 7],
                ["4" : 35],
                ["5" : 30],
                ["6" : 15],
                ["7": 45],
                ["8": 15],
                ["9" : 30],
                ["10" : 7]
            ]
        
        let graph = GraphView(frame: CGRect(x: 0, y: 40, width: self.contentView.bounds.width, height: 150), data: myData)
        graph.backgroundColor = UIColor.white
        self.viewMain.addSubview(graph)
    }
    
    override func setSelected(_ selected: Bool, animated: Bool)
    {
        super.setSelected(selected, animated: animated)
    }
    
}
