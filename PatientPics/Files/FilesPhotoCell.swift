//
//  FilesPhotoCell.swift
//  PatientPics
//
//  Created by Ankit Bagda on 03/08/19.
//  Copyright © 2019 Om Prakash Jangid. All rights reserved.
//

import UIKit

class FilesPhotoCell: UITableViewCell,UICollectionViewDataSource,UICollectionViewDelegate {
   

    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var dayLbl: UILabel!
    @IBOutlet weak var dateLbl: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    override func draw(_ rect: CGRect) {
        self.collectionView.dataSource = self
        self.collectionView.delegate = self
        
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 10
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "PhotoCollectionViewCell", for: indexPath as IndexPath) as! PhotoCollectionViewCell
        return cell
    }
    

}
