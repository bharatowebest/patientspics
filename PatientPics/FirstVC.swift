//
//  FirstVC.swift
//  PatientPics
//
//  Created by Ankit Bagda on 29/07/19.
//  Copyright © 2019 Om Prakash Jangid. All rights reserved.
//

import UIKit
import CarbonKit

class FirstVC: UIViewController,UITableViewDataSource,UITableViewDelegate {

    @IBOutlet weak var registerBtn: UIButton!
    @IBOutlet weak var registerBtnView: UIView!
    var imgarr = [#imageLiteral(resourceName: "login20"),#imageLiteral(resourceName: "login20"),#imageLiteral(resourceName: "user20"),#imageLiteral(resourceName: "user20"),#imageLiteral(resourceName: "email20"),#imageLiteral(resourceName: "call20"),#imageLiteral(resourceName: "location20"),#imageLiteral(resourceName: "lock20")]
    var txtFieldName = ["Practice(Medical Clinic)","Practice Name","First Name","Last Name","Email","Mobile Number","Address","Password"]
    
    @IBOutlet weak var tableView: UITableView!
    
    var items = ["1st","2nd"]
    var carbonTabSwipeNavigation: CarbonTabSwipeNavigation!


    override func viewDidLoad() {
        super.viewDidLoad()

        self.tableView.dataSource = self
        self.tableView.delegate = self
        
        registerBtn.layer.cornerRadius = 20
        registerBtnView.layer.cornerRadius = 25
        tableView.rowHeight = 50
        
     
        // Do any additional setup after loading the view.
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 8
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "RegisterCell", for: indexPath as IndexPath) as! RegisterCell
        cell.imageView?.image = imgarr[indexPath.row]
        cell.txtField.placeholder = txtFieldName[indexPath.row]
        
       // cell.txtField.attributedPlaceholder = NSAttributedString(string: "placeholder text",                                                     cell.txtField.attributes: [NSAttributedString.Key.foregroundColor: UIColor.lightGray])
        if indexPath.row == 0 || indexPath.row == 4
        {
            //cell.downArrowImg.image = #imageLiteral(resourceName: "arrow-down20")
        }
        return cell
    }
    
    @IBAction func regiteraction(_ sender: Any) {
        print("Register Clicked")
    }
}
