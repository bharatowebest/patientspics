//
//  Extensions.swift
//  PatientPics
//
//  Created by jai kalra on 09/08/19.
//  Copyright © 2019 owebest. All rights reserved.
//

import Foundation
import UIKit

extension UIView
{
    
    func dropShadow(_ color:UIColor = UIColor.lightGray)
    {
        layer.shadowColor = color.cgColor
        layer.shadowOpacity = 1
        layer.shadowOffset = CGSize.zero
        layer.shadowRadius = 5
    }
    

    func addBottomBorder(borderWidth:CGFloat,borderColor:UIColor)
    {
        
        let borderHeight = borderWidth
        let layer1 = CALayer()
        layer1.frame = CGRect(x: 0, y: frame.height - borderHeight, width: frame.width, height: frame.height)
        layer1.borderColor = borderColor.cgColor
        layer1.borderWidth = borderHeight
        self.layer.addSublayer(layer1)
        layer.masksToBounds = true
    }
    
    
}
