//
//  MyTextField.swift
//  PatientPics
//
//  Created by jai kalra on 12/08/19.
//  Copyright © 2019 owebest. All rights reserved.
//

import Foundation
import UIKit


class MytextField: UITextField {
    
    
    override func draw(_ rect: CGRect) {
       
    }
    
    @IBInspectable var showBottomBorder:Bool = false
    {
        didSet{
            if showBottomBorder
            {
                self.borderStyle = .none
                self.textColor = .black
                let border = CALayer()
                let width = CGFloat(1.0)
                border.borderColor = color.cgColor
                border.frame = CGRect(x: 0, y: self.frame.size.height - width,   width:  self.frame.size.width, height: self.frame.size.height)
                border.borderWidth = width
                self.layer.addSublayer(border)
                self.layer.masksToBounds = true
            }
        }
    }
    
    @IBInspectable var color: UIColor = .gray {
        didSet {
            setBottomBorder(color:color )
        }
    }
    
    func setBottomBorder(color:UIColor) {
        
    }
    
    
    
    

//    
    @IBInspectable var rightImage: String = "" {
        didSet {
            setRightImage()
        }
    }
    func setRightImage(){
        let DropDownEmailVw = UIImageView()
        DropDownEmailVw.frame = CGRect(x: 10,y: 10,width: 30,height: 20)
        DropDownEmailVw.image = UIImage(named: rightImage)
        DropDownEmailVw.contentMode = .center
        self.rightViewMode = UITextField.ViewMode.always
        self.rightView = DropDownEmailVw
        
    }
    
    @IBInspectable var leftImage: String = "" {
        didSet {
            setLeftImage()
        }
    }
    func setLeftImage(){
        let DropDownEmailVw = UIImageView()
        DropDownEmailVw.frame = CGRect(x: 10,y: 10,width: 30,height: 20)
        DropDownEmailVw.image = UIImage(named: leftImage)
        DropDownEmailVw.contentMode = .center
        self.leftViewMode = UITextField.ViewMode.always
        self.leftView = DropDownEmailVw
        
    }
    
    
    
    @IBInspectable var leftSpace: CGFloat = 0 {
        didSet {
            setleftSpace()
        }
    }
    func setleftSpace(){
        let DropDownEmailVw = UIImageView()
        DropDownEmailVw.frame = CGRect(x: 10,y: 10,width: leftSpace,height: 20)
        DropDownEmailVw.contentMode = .center
        self.leftViewMode = UITextField.ViewMode.always
        self.leftView = DropDownEmailVw
        
    }
    
    @IBInspectable var attributePlaceHolder: String = "" {
        
        didSet {
            setAttributePlaceHolder()
        }
    }
    
    @IBInspectable var placeHolderColor: UIColor = UIColor.white {
        didSet {
            setAttributePlaceHolder()
        }
    }
    
    func setAttributePlaceHolder(){
        self.attributedPlaceholder = NSAttributedString(string:attributePlaceHolder,attributes:[NSAttributedString.Key.foregroundColor: placeHolderColor]);
    }
    
    
    
    /*
     // Only override draw() if you perform custom drawing.
     // An empty implementation adversely affects performance during animation.
     override func draw(_ rect: CGRect) {
     // Drawing code
     }
     */
    
}
