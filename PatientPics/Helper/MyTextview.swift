//
//  MyImageView.swift
//  1FOODit
//
//  Created by Ankit Bagda on 15/03/19.
//  Copyright © 2019 OWeBest.com. All rights reserved.
//

import Foundation
import UIKit

class MyTextview:UITextView
{
    
    
    
    
    @IBInspectable var Circle:Bool = false{
        didSet{
            makeCircle()
        }
    }
    
    func makeCircle()
    {
        if Circle{
            
            self.layer.cornerRadius = self.frame.height / 2
        }
    }
//    @IBInspectable var borderColor: UIColor = UIColor.clear {
//        didSet {
//            setBorderColor()
//        }
//    }
//    func setBorderColor(){
//        self.layer.borderColor = borderColor.cgColor
//    }
//    
//    @IBInspectable var borderWidth: CGFloat = 0 {
//        didSet {
//            setBorderWidth()
//        }
//    }
//    func setBorderWidth(){
//        self.layer.borderWidth = borderWidth
//    }
//    
//    @IBInspectable var cornerRadious: CGFloat = 0 {
//        didSet {
//            setCornerRadious()
//        }
//    }
//    func setCornerRadious(){
//        self.layer.cornerRadius = cornerRadious
//    }
//    @IBInspectable var addShoadow:Bool = false{
//        didSet{
//            addShadowOnView()
//        }
//    }
//    func addShadowOnView()
//    {
//        self.dropShadow()
//    }
}

