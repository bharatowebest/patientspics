
//  MyButton.swift
//  PatientPics
//
//  Created by jai kalra on 09/08/19.
//  Copyright © 2019 OWeBest.com. All rights reserved.
//

import Foundation
import UIKit



class MyView: UIView {
    
    @IBInspectable var addShadow:Bool = false
    {
        didSet
        {
            if addShadow
            {
                dropShadow()
            }
        }
    }
    
    
    @IBInspectable var MakeCircle:Bool = false
    {
        didSet
        {
            if MakeCircle
            {
                self.layer.cornerRadius = self.frame.height / 2
            }
        }
    }


    @IBInspectable var cornerRadious: CGFloat = 0
    {
        didSet {
            setCornerRadious()
        }
    }
    
    @IBInspectable var BorderColor: UIColor = UIColor.white {
        didSet {
            setBorderColor()
        }
    }
    @IBInspectable var BorderWidth: CGFloat = 0 {
        didSet {
            setBorderColor()
        }
    }

    
    func setCornerRadious()
    {
        self.layer.cornerRadius = cornerRadious
    }
    
    func setBorderColor()
    {
        self.layer.borderColor = BorderColor.cgColor
        self.layer.borderWidth = BorderWidth
    }
    
    
    
}
