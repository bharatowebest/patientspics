//
//  UserDefault.swift
//  PatientPics
//
//  Created by jai kalra on 09/08/19.
//  Copyright © 2019 owebest. All rights reserved.
//

import Foundation


extension UserDefaults
{
    func setLogin(_ islogin:Bool)
    {
        set(islogin, forKey: "Login")
    }
    
    func getLogin()->Bool
    {
        return bool(forKey: "Login")
    }
}
