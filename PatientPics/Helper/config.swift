//
//  config.swift
//  PatientPics
//
//  Created by jai kalra on 09/08/19.
//  Copyright © 2019 owebest. All rights reserved.
//

import Foundation
import UIKit

class config
{
    static public let appThemeColor = UIColor(red: 0.1960784314, green: 0.7960784314, blue: 0.7843137255, alpha: 1)
}
