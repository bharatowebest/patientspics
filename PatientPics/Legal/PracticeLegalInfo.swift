//
//  PracticeLegalInfo.swift
//  PatientPics
//
//  Created by Ankit Bagda on 07/08/19.
//  Copyright © 2019 Om Prakash Jangid. All rights reserved.
//

import UIKit

class PracticeLegalInfo: UIViewController {

    @IBOutlet weak var saveBtn: UIButton!
    @IBOutlet weak var cancelBtn: UIButton!
    @IBOutlet weak var PracticeLegalEntityNameFirstTxtField: UITextField!
    
    @IBOutlet weak var emailTextField: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        PracticeLegalEntityNameFirstTxtField.layer.borderWidth = 0.8
        PracticeLegalEntityNameFirstTxtField.layer.borderColor = #colorLiteral(red: 0.8787946767, green: 0.8787946767, blue: 0.8787946767, alpha: 1)
        emailTextField.layer.borderWidth = 0.8
        emailTextField.layer.borderColor = #colorLiteral(red: 0.8787946767, green: 0.8787946767, blue: 0.8787946767, alpha: 1)
        
        saveBtn.layer.cornerRadius = saveBtn.frame.height / 2
        saveBtn.layer.borderColor = config.appThemeColor.cgColor
        saveBtn.layer.borderWidth = 1
        cancelBtn.layer.cornerRadius = saveBtn.frame.height / 2
        cancelBtn.layer.borderWidth = 1
        cancelBtn.layer.borderColor = config.appThemeColor.cgColor

        
    }
    override func viewWillAppear(_ animated: Bool)
    {
        self.title = "Settings"
        let menuBarButtonItem = UIBarButtonItem(image: UIImage(named: "menu-button-2.png"), style: .plain, target: self, action: #selector(menuBtn))
        self.navigationItem.leftBarButtonItem  = menuBarButtonItem
        
        var tabview = UIView()
        tabview = UIView(frame: CGRect(x: 0, y: 0, width: 80, height: 40))
        
        let notificationBarButtonItem = UIButton(frame: CGRect(x: 60, y:5, width: 30, height: 30))
        notificationBarButtonItem.setImage(UIImage(named: "bell20.png"), for: .normal)
        notificationBarButtonItem.addTarget(self, action: #selector(self.notificationBarButtonItemAction), for: UIControl.Event.touchUpInside)
        
        let addBarButtonItem = UIButton(frame: CGRect(x: 40, y: 5, width: 16, height: 16))
        addBarButtonItem.setImage(UIImage(named: "add20w.png"), for: .normal)
        addBarButtonItem.addTarget(self, action: #selector(self.addBarButtonItemAction), for: UIControl.Event.touchUpInside)
        
        let searchBarButtonItem = UIButton(frame: CGRect(x: 20, y: 5, width: 16, height: 16))
        searchBarButtonItem.setImage(UIImage(named: "search20.png"), for: .normal)
        searchBarButtonItem.addTarget(self, action: #selector(self.searchBarButtonItemAction), for: UIControl.Event.touchUpInside)
        
        
        var notificationLabel = UILabel()
        notificationLabel = UILabel(frame: CGRect(x: 77, y: 3, width: 16, height: 16))
        notificationLabel.backgroundColor = UIColor.red
        notificationLabel.layer.cornerRadius = notificationLabel.frame.height / 2
        notificationLabel.layer.masksToBounds = true
        notificationLabel.text = "3"
        notificationLabel.textAlignment = .center
        notificationLabel.font = UIFont.boldSystemFont(ofSize: 16)
        notificationLabel.textColor = UIColor.white
        tabview.addSubview(notificationBarButtonItem)
        tabview.addSubview(notificationLabel)
        tabview.addSubview(addBarButtonItem)
        tabview.addSubview(searchBarButtonItem)
        
        navigationController?.navigationBar.tintColor = .white
        let rightBarButton = UIBarButtonItem(customView: tabview)
        self.navigationItem.rightBarButtonItem = rightBarButton
    }
    
    @objc func menuBtn()
    {
        sideMenuController?.showLeftViewAnimated()
    }
    
    @objc func  notificationBarButtonItemAction()
    {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "NotificationListVC") as! NotificationListVC
        navigationController?.pushViewController(vc, animated: true)
    }
    
    @objc func  addBarButtonItemAction()
    {
        
    }
    @objc func searchBarButtonItemAction()
    {
        
    }

    @IBAction func cancelAction(_ sender: Any) {
        cancelBtn.backgroundColor = config.appThemeColor
        cancelBtn.setTitleColor(UIColor.white, for: .normal)
        saveBtn.backgroundColor = UIColor.white
        saveBtn.titleLabel?.textColor = config.appThemeColor
        
    }
    @IBAction func saveAction(_ sender: Any) {
        saveBtn.backgroundColor = config.appThemeColor
        saveBtn.titleLabel?.textColor = UIColor.white
        cancelBtn.backgroundColor = UIColor.white
        cancelBtn.titleLabel?.textColor = config.appThemeColor


    }
}
