//
//  LoginAndRegisterVC.swift
//  PatientPics
//
//  Created by jai kalra on 12/08/19.
//  Copyright © 2019 owebest. All rights reserved.
//

import UIKit
import DropDown

class LoginAndRegisterVC: UIViewController {

    
    @IBOutlet weak var registerSmallView: UIView!
    @IBOutlet weak var registerButton: MyButton!
    @IBOutlet weak var loginButton: MyButton!
    @IBOutlet weak var registerBackView: UIView!
    
    @IBOutlet weak var userTypeTextField: MytextField!
    @IBOutlet weak var praticeNameTextField: MytextField!
    @IBOutlet weak var firstName: MytextField!
    @IBOutlet weak var lastName: MytextField!
    @IBOutlet weak var emailTextField: MytextField!
    @IBOutlet weak var mobileTextField: MytextField!
    @IBOutlet weak var addressTextField: MytextField!
    @IBOutlet weak var passwordTextField: MytextField!
    
    let userTypeDropDown = DropDown()
    

    override func viewDidLoad() {
        super.viewDidLoad()
        setView()
        registerButton.isSelected = true
        registerButton.addBottomBorder(borderWidth: 5, borderColor: config.appThemeColor)
        loginButton.addBottomBorder(borderWidth: 0.5, borderColor: UIColor.black)
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.navigationController?.navigationBar.isHidden = true
        self.registerBackView.transform = CGAffineTransform.identity.scaledBy(x: 1, y: 0.7)
        UIView.animate(withDuration: 0.5, delay: 0.0, options: UIView.AnimationOptions.curveEaseIn, animations: {
            self.registerBackView.transform = CGAffineTransform.identity.scaledBy(x: 1, y: 1) // Scale your image
        }) { (finished) in
        }
    }
    
    func setView()
    {
        let swipeGestuere = UISwipeGestureRecognizer(target: self, action: #selector(swipePerform))
        registerBackView.isUserInteractionEnabled = true
        registerBackView.addGestureRecognizer(swipeGestuere)
        swipeGestuere.direction = .left
        userTypeDropDown.anchorView = userTypeTextField
        userTypeDropDown.dataSource = ["Practice","Doctor"]
        userTypeDropDown.bottomOffset = CGPoint(x: 0, y:(userTypeDropDown.anchorView?.plainView.bounds.height)!)
        userTypeDropDown.selectionAction = { [unowned self] (index: Int, item: String) in
            self.userTypeDropDown.hide()
            print("Selected item: \(item) at index: \(index)")
            self.userTypeTextField.text = item
        }
        
        
    }
    
    @objc func swipePerform()
    {
        let vc = LoginVc(nibName: "LoginVc", bundle: nil)
        self.navigationController?.pushViewController(vc, animated: false)
    }

    

    @IBAction func registerButonPress(_ sender: UIButton) {
    }
    
    
    @IBAction func loginButtonPress(_ sender: UIButton) {
        
        let vc = LoginVc(nibName: "LoginVc", bundle: nil)
        self.navigationController?.pushViewController(vc, animated: false)
    }
    
    
    
    @IBAction func userRegisterButtonPress(_ sender: UIButton) {
        guard checkValidation() else {
            return
        }
        
        let roleId = userTypeTextField.text == "Doctor" ? 3 : 2
        var parameter = [String:Any]()
        parameter["role_id"] = roleId
        parameter["fname"] = firstName.text
        parameter["lname"] = lastName.text
        parameter["practice_name"] = praticeNameTextField.text
        parameter["phone"] = mobileTextField.text
        parameter["email"] = emailTextField.text
        parameter["password"] = passwordTextField.text
        showLoader()
        postRequest(strUrl: "register", param: parameter, success: { (json) in
            print(json)
            self.hideLoader()
            let msg = json["message"].rawString() ?? ""
            self.view.makeToast(message: msg)
        }) { (msg) in
            self.ShowAlert(message: msg)
        }
        
        
        
        
    }
    
    func checkValidation()->Bool
    {
        guard userTypeTextField.text != "" else {
            self.view.makeToast(message: "Please Enter User Type")
            return false
        }
        guard praticeNameTextField.text != "" else {
            self.view.makeToast(message: "Please Enter Pratice Name")
            return false
        }
        guard firstName.text != "" else {
            self.view.makeToast(message: "Please Enter First Name")
            return false
        }
        guard lastName.text != "" else {
            self.view.makeToast(message: "Please Enter Last Name")
            return false
        }
        guard emailTextField.text != "" else {
            self.view.makeToast(message: "Please Enter Email")
            return false
        }
        guard mobileTextField.text != "" else {
            self.view.makeToast(message: "Please Enter Mobile Number")
            return false
        }
        guard passwordTextField.text != "" else {
            self.view.makeToast(message: "Please Enter Password")
            return false
        }
        
        return true
        
        
        
    }
    
    
}


extension LoginAndRegisterVC:UITextFieldDelegate
{
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if textField == self.userTypeTextField
        {
            textField.resignFirstResponder()
            textField.endEditing(true)
            self.userTypeDropDown.show()
            
        }
    }
}
