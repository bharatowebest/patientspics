//
//  LoginVc.swift
//  PatientPics
//
//  Created by jai kalra on 12/08/19.
//  Copyright © 2019 owebest. All rights reserved.
//

import UIKit
import DropDown

class LoginVc: UIViewController {
    
    
    
    //api responce handler start:-
    var loginDataModal: loginModal!
    
    
    @IBOutlet weak var registerButton: MyButton!
    @IBOutlet weak var loginButton: MyButton!
    @IBOutlet weak var loginView: UIView!
    
    @IBOutlet weak var userTypeTextField: MytextField!
    @IBOutlet weak var emailTextField: MytextField!
    @IBOutlet weak var passwordTextField: MytextField!
    
    var userTypeDropDown = DropDown()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setView()
        loginButton.isSelected = true
        registerButton.addBottomBorder(borderWidth: 0.5, borderColor: UIColor.black)
        loginButton.addBottomBorder(borderWidth: 5, borderColor: config.appThemeColor)
        // Do any additional setup after loading the view.
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.loginView.transform = CGAffineTransform.identity.scaledBy(x: 1, y: 1.5)
        UIView.animate(withDuration: 0.4, delay: 0.0, options: UIView.AnimationOptions.curveEaseIn, animations: {
            self.loginView.transform = CGAffineTransform.identity.scaledBy(x: 1, y: 1) // Scale your image
        }) { (finished) in
            
        }
    }
    

    func setView()
    {
        let swipeGestuere = UISwipeGestureRecognizer(target: self, action: #selector(swipePerform))
        loginView.isUserInteractionEnabled = true
        loginView.addGestureRecognizer(swipeGestuere)
        swipeGestuere.direction = .right
        
        userTypeDropDown.anchorView = userTypeTextField
        userTypeDropDown.dataSource = ["Practice","Patient","Doctor"]
        userTypeDropDown.bottomOffset = CGPoint(x: 0, y:(userTypeDropDown.anchorView?.plainView.bounds.height)!)
        userTypeDropDown.selectionAction = { [unowned self] (index: Int, item: String) in
            self.userTypeDropDown.hide()
            print("Selected item: \(item) at index: \(index)")
            self.userTypeTextField.text = item
        }
        
    }
    
    @objc func swipePerform()
    {
        self.navigationController?.popViewController(animated: false)
    }
    @IBAction func RegisterButtonPress(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: false)
        
    }
    @IBAction func forgotPasswordButtonPress(_ sender: Any) {
        let controller = forgotPasswordVC(nibName: "forgotPasswordVC", bundle: nil)
        addChild(controller)
        controller.view.backgroundColor = UIColor(white: 0, alpha: 0.5)
        controller.view.frame = view.bounds
        view.addSubview(controller.view)
        controller.didMove(toParent: self)
        
    }
    
    @IBAction func userLoginButtonPress(_ sender: Any) {
        
        guard checkValidation() else {
            return
        }
        
        let roleId = userTypeTextField.text == "Doctor" ? 3 : (userTypeTextField.text == "Patient") ? 4 : 2
        
        var parameter = [String:Any]()
        parameter["role_id"] = roleId
        parameter["email"] = emailTextField.text
        parameter["password"] = passwordTextField.text
        showLoader()
        postRequest(strUrl: "login", param: parameter, success: { (json) in
            print(json)
            self.hideLoader()
            self.loginDataModal = loginModal(fromJson: json)
            if self.loginDataModal.error == true
            {
                self.ShowAlert(message: self.loginDataModal.message)
                return
            }
            UserDefaults.standard.set(self.loginDataModal.userId, forKey: "user_id")
            UserDefaults.standard.set(self.loginDataModal.data.token, forKey: "token")
            UserDefaults.standard.setLogin(true)
            let window = UIApplication.shared.keyWindow
            let storyBoard = UIStoryboard(name: "Main", bundle: nil)
            let dashboard = storyBoard.instantiateViewController(withIdentifier: "LGSideMenuController")
            window?.rootViewController = dashboard
            
        }) { (msg) in
            self.ShowAlert(message: msg)
        }
        
        
        
        

        
        
    }
    
    func checkValidation()->Bool
    {
        guard userTypeTextField.text != "" else {
            self.view.makeToast(message: "Please Enter User Type")
            return false
        }
        guard emailTextField.text != "" else {
            self.view.makeToast(message: "Please Enter Email")
            return false
        }
        guard passwordTextField.text != "" else {
            self.view.makeToast(message: "Please Enter Password")
            return false
        }
        
        return true
        
        
        
    }
   
    
    
}


extension LoginVc:UITextFieldDelegate
{
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if textField == self.userTypeTextField
        {
            textField.resignFirstResponder()
            textField.endEditing(true)
            self.userTypeDropDown.show()
            
        }
    }
}


