//
//  forgotPasswordVC.swift
//  PatientPics
//
//  Created by jai kalra on 13/08/19.
//  Copyright © 2019 Om Prakash Jangid. All rights reserved.
//

import UIKit

class forgotPasswordVC: UIViewController {

    @IBOutlet weak var emailTextField: UITextField!

    override func viewDidLoad() {
        super.viewDidLoad()
    }


    @IBAction func cancelButtonPress(_ sender: Any) {
        self.view.removeFromSuperview()
        
        
    }
    
    
    @IBAction func submitButtonPress(_ sender: Any)
    {
        guard emailTextField.text != "" else {
            self.view.makeToast(message: "Please Enter Email")
            return
        }
        
        var parameter = [String:Any]()
        
        parameter["email"] = emailTextField.text
        showLoader()
        postRequest(strUrl: "forget-password", param: parameter, success: { (json) in
            print(json)
            self.hideLoader()
            let msg = json["message"].rawString() ?? ""
            self.view.makeToast(message: msg)
            self.view.removeFromSuperview()
            
        }) { (msg) in
            self.ShowAlert(message: msg)
        }
    
    }
   
    
    
    
}
