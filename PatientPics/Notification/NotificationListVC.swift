//
//  NotificationListVC.swift
//  PatientPics
//
//  Created by Ankit Bagda on 12/08/19.
//  Copyright © 2019 Om Prakash Jangid. All rights reserved.
//

import UIKit

class NotificationListVC: UIViewController, UITableViewDataSource, UITableViewDelegate {
    
    @IBOutlet weak var tableView: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool)
    {
        self.title = "Notifications"
        super.viewWillAppear(true)
        self.navigationItem.leftBarButtonItem = nil
        self.navigationController?.navigationBar.isHidden = false
        let backBarButtonItem = UIBarButtonItem(image: UIImage(named:"back-20w.png"), style: .plain, target: self, action: #selector(backBtn))
        self.navigationItem.leftBarButtonItem  = backBarButtonItem
        navigationController?.navigationBar.tintColor = .white
       
    }
   
    @objc func backBtn()
    {
        navigationController?.popViewController(animated: true)
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 20
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "notificationListCell", for: indexPath as IndexPath) as! notificationListCell
      
        cell.contentView.backgroundColor = UIColor.white
        cell.contentView.dropShadow()

        return cell
    }
    
}
extension UIView {
    
//    func setCardView(view : UIView)
//    {
//        view.layer.cornerRadius = 5.0
//        view.layer.borderColor  =  UIColor.lightGray.cgColor
//        view.layer.borderWidth = 5.0
//        view.layer.shadowOpacity = 0.5
//        view.layer.shadowColor =  UIColor.lightGray.cgColor
//        view.layer.shadowRadius = 15.0
//        view.layer.shadowOffset = CGSize(width:5, height: 5)
//        view.layer.masksToBounds = true
//    }
}

