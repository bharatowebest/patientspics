//
//  AddNewPatient.swift
//  PatientPics
//
//  Created by Ankit Bagda on 16/08/19.
//  Copyright © 2019 Om Prakash Jangid. All rights reserved.
//

import UIKit

class AddNewPatient:UIViewController, UIPickerViewDelegate, UITextFieldDelegate,UIPickerViewDataSource
{
    
        @IBOutlet weak var emailTextField: UITextField!
        @IBOutlet weak var firstNameTextField: UITextField!
        @IBOutlet weak var lastNameTextField: UITextField!
        @IBOutlet weak var userImgView: UIImageView!
        @IBOutlet weak var phoneNumberTextField: UITextField!
        @IBOutlet weak var dateOfBirthTextField: UITextField!
        @IBOutlet weak var editBtn: UIButton!
        @IBOutlet weak var saveBtn: UIButton!
        @IBOutlet weak var passwordTextField: UITextField!
        @IBOutlet weak var genderTextField: UITextField!
        @IBOutlet weak var cancleBtn: UIButton!
        let gender = ["Male", "Female", "Other"]
        var getGender: UIPickerView! = UIPickerView()
        
        let datePickerView: UIDatePicker! = UIDatePicker()
        
        
        override func viewDidLoad() {
            super.viewDidLoad()
            let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(UIInputViewController.dismissKeyboard))
            view.addGestureRecognizer(tap)
            
            userImgView.layer.cornerRadius = userImgView.frame.height / 2
            editBtn.layer.cornerRadius = editBtn.frame.height / 2
            dateOfBirthTextField.delegate = self
            self.dateOfBirthTextField.inputView = datePickerView
            
            
            self.genderTextField.inputView = getGender
            getGender.dataSource = self
            getGender.delegate = self
            
            
            saveBtn.layer.cornerRadius = saveBtn.frame.height / 2
            saveBtn.borderWidthe = 1
            saveBtn.borderColoer = UIColor(displayP3Red: 63.0/255.0, green: 200.0/255.0, blue: 190.0/255.0, alpha: 1.0)
            cancleBtn.layer.cornerRadius = cancleBtn.frame.height / 2
            cancleBtn.borderWidthe = 1
            
            cancleBtn.borderColoer = UIColor(displayP3Red: 63.0/255.0, green: 200.0/255.0, blue: 190.0/255.0, alpha: 1.0)
            getGender.frame = CGRect(x: 10, y: 0, width: 50, height: 150)
            rightImgView()
        }
        
        
        func  rightImgView()
        {
            let imgView = UIView()
            imgView.frame = CGRect(x: 0, y: 0, width: 30, height: 20)
            let arrowButton = UIButton()
            arrowButton.frame = CGRect(x: 0, y: 7, width: 10, height: 10)
            arrowButton.setBackgroundImage(UIImage(named: "arrow-down20.png"), for: .normal)
            imgView.addSubview(arrowButton)
            genderTextField.rightView = imgView
            genderTextField.rightViewMode = .always
            
        }
        
        override func viewWillAppear(_ animated: Bool) {
            super.viewWillAppear(true)
            self.navigationItem.leftBarButtonItem = nil
            self.navigationController?.navigationBar.isHidden = false
            self.title = "Add Patient"
            let backBarButtonItem = UIBarButtonItem(image: UIImage(named: "back-20w.png"), style: .plain, target: self, action: #selector(backBtn))
            self.navigationItem.leftBarButtonItem  = backBarButtonItem
            
            var tabview = UIView()
            tabview = UIView(frame: CGRect(x: 0, y: 0, width: 80, height: 30))
            
            let notificationBarButtonItem = UIButton(frame: CGRect(x: 60, y: 0, width: 30, height: 30))
            notificationBarButtonItem.setImage(UIImage(named: "bell20.png"), for: .normal)
            notificationBarButtonItem.addTarget(self, action: #selector(self.notificationBarButtonItemAction), for: UIControl.Event.touchUpInside)
            
            var notificationLabel = UILabel()
            notificationLabel = UILabel(frame: CGRect(x: 77, y: 3, width: 16, height: 16))
            notificationLabel.backgroundColor = UIColor.red
            notificationLabel.layer.cornerRadius = notificationLabel.frame.height / 2
            notificationLabel.layer.masksToBounds = true
            notificationLabel.text = "3"
            notificationLabel.textAlignment = .center
            notificationLabel.font = UIFont.boldSystemFont(ofSize: 16)
            notificationLabel.textColor = UIColor.white
            tabview.addSubview(notificationBarButtonItem)
            tabview.addSubview(notificationLabel)
            
            let rightBarButton = UIBarButtonItem(customView: tabview)
            self.navigationItem.rightBarButtonItem = rightBarButton
        }
        
        @objc func backBtn()
        {
            navigationController?.popViewController(animated: true)
            
        }
        
        @objc func  notificationBarButtonItemAction()
        {
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "NotificationListVC") as! NotificationListVC
            navigationController?.pushViewController(vc, animated: true)
        }
        
        
        
        func numberOfComponents(in pickerView: UIPickerView) -> Int {
            return 1
        }
        func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int{
            return gender.count
        }
        
        func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
            return gender[row]
        }
        
        func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int)
        {
            genderTextField.text = "\(gender[row])"
            //        self.view.endEditing(true)
            
        }
        @objc func dismissKeyboard() {
            //Causes the view (or one of its embedded text fields) to resign the first responder status.
            view.endEditing(true)
        }
        
        
        @IBAction func cancelBtnAction(_ sender: Any) {
            cancleBtn.backgroundColor = UIColor(displayP3Red: 63.0/255.0, green: 200.0/255.0, blue: 190.0/255.0, alpha: 1.0)
            cancleBtn.setTitleColor(UIColor.white, for: .normal)
            
            saveBtn.backgroundColor = UIColor.white
            saveBtn.setTitleColor(UIColor(displayP3Red: 63.0/255.0, green: 200.0/255.0, blue: 190.0/255.0, alpha: 1.0), for: .normal)
        }
        @IBAction func saveBtnAction(_ sender: Any) {
            saveBtn.backgroundColor = UIColor(displayP3Red: 63.0/255.0, green: 200.0/255.0, blue: 190.0/255.0, alpha: 1.0)
            saveBtn.setTitleColor(UIColor.white, for: .normal)
            cancleBtn.backgroundColor = UIColor.white
            cancleBtn.setTitleColor(UIColor(displayP3Red: 63.0/255.0, green: 200.0/255.0, blue: 190.0/255.0, alpha: 1.0), for: .normal)
        }
        
        @IBAction func dateTextFieldAction(_ sender: Any)
        {
            datePickerView.datePickerMode = UIDatePicker.Mode.date
            datePickerView.addTarget(self, action: #selector(datePickerValueChanged), for: UIControl.Event.valueChanged)
        }
        
        @objc func datePickerValueChanged(sender:UIDatePicker)
        {
            let dateFormatter = DateFormatter()
            dateFormatter.dateStyle = DateFormatter.Style.medium
            dateFormatter.timeStyle = DateFormatter.Style.none
            dateOfBirthTextField.text = dateFormatter.string(from: sender.date)
        }
        
        
}
