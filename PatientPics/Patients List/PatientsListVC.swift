//
//  PatientsListVC.swift
//  PatientPics
//
//  Created by Ankit Bagda on 02/08/19.
//  Copyright © 2019 Om Prakash Jangid. All rights reserved.
//

import UIKit
import LGSideMenuController

class PatientsListVC: UIViewController,UITableViewDataSource,UITableViewDelegate {
  
    @IBOutlet weak var filterDownArraowBtn: UIButton!
    @IBOutlet weak var filterLbl: UILabel!
    @IBOutlet weak var tableview: UITableView!
    var cellMenuClickView:UIView = UIView()
     let blackview = UIView()
    var editBtn:UIButton = UIButton()
    var makeInactiveBtn:UIButton = UIButton()
    var deleteBtn:UIButton = UIButton()
    var SendResetPasswordLink:UIButton = UIButton()
    var viewY = Int()
    var v:UIView = UIView()
    var data = [1,2,3,4,5,6,7,8,9,10]
    
    var lastIndex = Int()
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        tableview.rowHeight = 75
        filterLbl.layer.cornerRadius = filterLbl.frame.height / 2
        filterLbl.layer.borderWidth = 0.7
        filterLbl.layer.borderColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.navigationItem.leftBarButtonItem = nil
        self.navigationController?.navigationBar.isHidden = false
        self.title = "Patients"
        let backBarButtonItem = UIBarButtonItem(image: UIImage(named: "menu-button-2.png"), style: .plain, target: self, action: #selector(backBtn))
        self.navigationItem.leftBarButtonItem  = backBarButtonItem
       
        var tabview = UIView()
        tabview = UIView(frame: CGRect(x: 0, y: 0, width: 80, height: 30))
        
        let notificationBarButtonItem = UIButton(frame: CGRect(x: 60, y: 0, width: 30, height: 30))
        notificationBarButtonItem.setImage(UIImage(named: "bell20.png"), for: .normal)
        notificationBarButtonItem.addTarget(self, action: #selector(self.notificationBarButtonItemAction), for: UIControl.Event.touchUpInside)

        let addBarButtonItem = UIButton(frame: CGRect(x: 30, y: 0, width: 30, height: 30))
        addBarButtonItem.setImage(UIImage(named: "add20w"), for: .normal)
        addBarButtonItem.addTarget(self, action: #selector(self.addBarButtonItemAction), for: UIControl.Event.touchUpInside)

        let searchBarButtonItem = UIButton(frame: CGRect(x: 0, y: 0, width: 30, height: 30))
        searchBarButtonItem.setImage(UIImage(named: "search20.png"), for: .normal)
        searchBarButtonItem.addTarget(self, action: #selector(self.searchBarButtonItemAction), for: UIControl.Event.touchUpInside)
        
        var notificationLabel = UILabel()
        notificationLabel = UILabel(frame: CGRect(x: 77, y: 2, width: 16, height: 16))
        notificationLabel.backgroundColor = UIColor.red
        notificationLabel.layer.cornerRadius = notificationLabel.frame.height / 2
        notificationLabel.layer.masksToBounds = true
        notificationLabel.text = "3"
        notificationLabel.textAlignment = .center
        notificationLabel.font = UIFont.boldSystemFont(ofSize: 16)
        notificationLabel.textColor = UIColor.white
        navigationController?.navigationBar.tintColor = .white
        
        tabview.addSubview(notificationBarButtonItem)
        tabview.addSubview(addBarButtonItem)
        tabview.addSubview(searchBarButtonItem)
        tabview.addSubview(notificationLabel)
        
        let rightBarButton = UIBarButtonItem(customView: tabview)
        self.navigationItem.rightBarButtonItem = rightBarButton
    }
    
    @objc func backBtn()
    {
        sideMenuController?.showLeftViewAnimated()
    }

    @objc func notificationBarButtonItemAction()
    {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "NotificationListVC") as! NotificationListVC
        navigationController?.pushViewController(vc, animated: true)
    }
    
    @objc func  addBarButtonItemAction()
    {
        let secondViewController = self.storyboard?.instantiateViewController(withIdentifier: "AddNewPatient") as! AddNewPatient
        self.navigationController?.pushViewController(secondViewController, animated: true)
        
    }
    
    @objc func  searchBarButtonItemAction()
    {
        
    }
    
    func cellMenuClickViewShow(view:UIView , indexP : IndexPath)
    {
        blackview.frame = self.view.frame
        blackview.backgroundColor = UIColor(white: 0, alpha: 0.0)
        let window = UIApplication.shared.keyWindow!
       // let cell = tableview.cellForRow(at: indexP) as! PatientsListCell
        cellMenuClickView = UIView()
        cellMenuClickView.frame = CGRect.init(x: view.frame.width - 276, y: (view.superview?.superview!.frame.origin.y)!+126-tableview.contentOffset.y , width: 240, height: 180)
        cellMenuClickView.backgroundColor = UIColor.white     //give color to the view
        editBtn.frame = CGRect.init(x: self.cellMenuClickView.frame.width - 70, y: 20, width: 50, height: 20)
        editBtn.contentHorizontalAlignment = UIControl.ContentHorizontalAlignment.right
        editBtn.setTitleColor(UIColor.green, for: .normal)
        editBtn.setTitle("Edit", for: .normal)
        self.cellMenuClickView.addSubview(editBtn)
        
        makeInactiveBtn.frame = CGRect.init(x: self.cellMenuClickView.frame.width - 150, y: 60, width: 130, height: 20)
        makeInactiveBtn.contentHorizontalAlignment = UIControl.ContentHorizontalAlignment.right
        makeInactiveBtn.setTitleColor(#colorLiteral(red: 0.9254902005, green: 0.2352941185, blue: 0.1019607857, alpha: 1), for: .normal)
        makeInactiveBtn.setTitle("Make Inactive", for: .normal)
        self.cellMenuClickView.addSubview(makeInactiveBtn)
        
        deleteBtn.frame = CGRect.init(x: self.cellMenuClickView.frame.width - 80, y: 100, width: 60, height: 20)
        deleteBtn.contentHorizontalAlignment = UIControl.ContentHorizontalAlignment.right
        deleteBtn.setTitleColor(#colorLiteral(red: 0.8078431487, green: 0.02745098062, blue: 0.3333333433, alpha: 1), for: .normal)
        deleteBtn.setTitle("Delete", for: .normal)
        self.cellMenuClickView.addSubview(deleteBtn)
        
        SendResetPasswordLink.frame = CGRect.init(x: self.cellMenuClickView.frame.width - 240, y: 140, width: 220, height: 20)
        SendResetPasswordLink.contentHorizontalAlignment = UIControl.ContentHorizontalAlignment.right
        SendResetPasswordLink.setTitleColor(UIColor.blue, for: .normal)
        SendResetPasswordLink.setTitle("Send reset Password Link", for: .normal)
        self.cellMenuClickView.addSubview(SendResetPasswordLink)
        
        if lastIndex == data.count - 1
        {
             cellMenuClickView.frame = CGRect.init(x: view.frame.width - 276, y: (view.superview?.superview!.frame.origin.y)!+20-tableview.contentOffset.y , width: 240, height: 180)
        }
        
        cellMenuClickView.dropShadow()
        window.addSubview(cellMenuClickView)
        print(window)
    }
    
    
    func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
         blackview.removeFromSuperview()
        cellMenuClickView.removeFromSuperview()
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return data.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell = tableview.dequeueReusableCell(withIdentifier: "PatientsListCell", for: indexPath as IndexPath) as!
        PatientsListCell
        cell.cellMenuBtn.tag = indexPath.row
       
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        
        if cellMenuClickView.window != nil {
             cellMenuClickView.removeFromSuperview()
        }
        else
        {
            let cell = tableview.dequeueReusableCell(withIdentifier: "PatientsListCell", for: indexPath as IndexPath) as!
            PatientsListCell
            
            if indexPath.row == data.count - 1
            {
                lastIndex = indexPath.row
            }
            let newView = UIView(frame: CGRect(x: 100, y: 50, width: 100, height: 200))
            cell.contentView.addSubview(newView)
            
            let secondViewController = self.storyboard?.instantiateViewController(withIdentifier: "rootViewController") as! rootViewController
            self.navigationController?.pushViewController(secondViewController, animated: true)
           
        }

        
    }
    
    @IBAction func cellMenuActionBtn(_ sender: UIButton)
    {
         blackview.removeFromSuperview()
        cellMenuClickView.removeFromSuperview()
        let a = sender.tag
        print(a)
        let indexpath = IndexPath(row: a, section: 0)
        let cell = tableview.cellForRow(at: indexpath) as! PatientsListCell
        print(cell)
        
         let v = cell.smallView
        cellMenuClickViewShow(view: v!, indexP: indexpath)

    //    v.addSubview(cellMenuClickView)
        
    }
    @IBAction func filterDownArraowBtnAction(_ sender: Any) {
    }
}

