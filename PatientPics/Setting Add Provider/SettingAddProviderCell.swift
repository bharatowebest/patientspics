//
//  SettingAddProviderCell.swift
//  PatientPics
//
//  Created by Ankit Bagda on 31/07/19.
//  Copyright © 2019 Om Prakash Jangid. All rights reserved.
//

import UIKit

class SettingAddProviderCell: UITableViewCell {

    @IBOutlet weak var nameTxtField: UITextField!
    @IBOutlet weak var nameLbl: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    override func draw(_ rect: CGRect) {
        nameTxtField.layer.borderWidth = 0.7
        nameTxtField.layer.borderColor = #colorLiteral(red: 0.4242420118, green: 0.4242420118, blue: 0.4242420118, alpha: 1)
    
    }
}
