//
//  SettingAddProviderVC.swift
//  PatientPics
//
//  Created by Ankit Bagda on 31/07/19.
//  Copyright © 2019 Om Prakash Jangid. All rights reserved.
//

import UIKit

class SettingAddProviderVC: UIViewController,UITableViewDelegate,UITableViewDataSource {
   
    var nameArr = ["First Name","Last Name","Email Id","Title"]
   var textFieldArr = ["Type here","Type here","Type here","Select here"]

    @IBOutlet weak var updateBtn: UIButton!
    @IBOutlet weak var tableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

      updateBtn.layer.cornerRadius = updateBtn.frame.height / 2
        tableView.rowHeight = 80
        
        
        // Do any additional setup after loading the view.
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 4
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "SettingAddProviderCell", for: indexPath as IndexPath) as? SettingAddProviderCell
    
        cell?.nameLbl.text = nameArr[indexPath.row]
      //  cell?.nameTxtField?.text = textFieldArr[indexPath.row]
     
        cell?.nameTxtField.attributedPlaceholder = NSAttributedString(string: textFieldArr[indexPath.row] ,
        attributes: [NSAttributedString.Key.foregroundColor: UIColor(displayP3Red: 200.0/255.0, green: 200.0/255.0, blue: 200.0/255.0, alpha: 1.0)])

        if indexPath.row == 3
        {
            var textView = UIView()
            textView = UIView(frame: CGRect(x: 0, y: 0, width: 20, height: 10))
            cell?.nameTxtField.addSubview(textView)
            
            let image = UIImage(named: "arrow-down20")
            let imageView = UIImageView(frame: CGRect(x:0, y: 0, width: 8  , height: 8))
            imageView.image = image
            textView.addSubview(imageView)
            cell?.nameTxtField.rightViewMode = UITextField.ViewMode.always
            cell?.nameTxtField.rightView = textView
        }
        return cell!
    }
    @objc func newButton()
    {
        print("hello")
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        print("dfdf")
    }
    
    @IBAction func closeBtn(_ sender: Any) {
        self.view.isHidden = true

    }
    
    @IBAction func update(_ sender: Any) {
       
    }
    
    
}
