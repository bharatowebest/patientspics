//
//  AdvanceSettingCell.swift
//  PatientPics
//
//  Created by Ankit Bagda on 07/08/19.
//  Copyright © 2019 Om Prakash Jangid. All rights reserved.
//

import UIKit

class AdvanceSettingCell: UITableViewCell {

    @IBOutlet weak var enableShowBtn: UIButton!
    @IBOutlet weak var imgUploadByPatientBtn: UIButton!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
