//
//  AdvanceSettingVC.swift
//  PatientPics
//
//  Created by Ankit Bagda on 07/08/19.
//  Copyright © 2019 Om Prakash Jangid. All rights reserved.
//

import UIKit

class AdvanceSettingVC: UIViewController,UITableViewDataSource,UITableViewDelegate
{
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var viewPopUp: UIView!
    var arrHeading = ["Image uploaded by Patients", "Auto Send Welcome Email"]
    
    override func viewDidLoad() {
        super.viewDidLoad()

    }
    override func viewWillAppear(_ animated: Bool) {
        
        self.title = "Dashboard"
        super.viewWillAppear(true)
        viewPopUp.isHidden = true
        self.navigationItem.leftBarButtonItem = nil
        self.navigationController?.navigationBar.isHidden = false
        let menuBarButtonItem = UIBarButtonItem(image: UIImage(named:"menu-button-2.png"), style: .plain, target: self, action: #selector(menuBtn))
        self.navigationItem.leftBarButtonItem  = menuBarButtonItem
        navigationController?.navigationBar.tintColor = .white
        
        tableView.frame = CGRect(x: 10, y: 81, width: tableView.frame.size.width, height: tableView.contentSize.height)
        tableView.layer.borderWidth = 0.3
        tableView.layer.borderColor = UIColor.darkGray.cgColor
        
        var tabview = UIView()
        tabview = UIView(frame: CGRect(x: 0, y: 0, width: 60, height: 40))
        
        let notificationBarButtonItem = UIButton(frame: CGRect(x: 40, y: 2, width: 30, height: 30))
        notificationBarButtonItem.setImage(UIImage(named: "bell20.png"), for: .normal)
        notificationBarButtonItem.addTarget(self, action: #selector(self.notificationBarButtonItemAction), for: UIControl.Event.touchUpInside)
        
        var notificationLabel = UILabel()
        notificationLabel = UILabel(frame: CGRect(x: 57, y: 4, width: 16, height: 16))
        notificationLabel.backgroundColor = UIColor.red
        notificationLabel.layer.cornerRadius = notificationLabel.frame.height / 2
        notificationLabel.layer.masksToBounds = true
        notificationLabel.text = "3"
        notificationLabel.textAlignment = .center
        notificationLabel.font = UIFont.boldSystemFont(ofSize: 16)
        notificationLabel.textColor = UIColor.white
        tabview.addSubview(notificationBarButtonItem)
        tabview.addSubview(notificationLabel)
        
        
        let rightBarButton = UIBarButtonItem(customView: tabview)
        self.navigationItem.rightBarButtonItem = rightBarButton
    }
    
    @objc func notificationBarButtonItemAction()
    {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "NotificationListVC") as! NotificationListVC
        navigationController?.pushViewController(vc, animated: true)
    }
    @objc func menuBtn()
    {
        sideMenuController?.showLeftViewAnimated()
    }
 
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrHeading.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "AdvanceSettingCell", for: indexPath as IndexPath) as! AdvanceSettingCell
        cell.imgUploadByPatientBtn.setTitle(arrHeading[indexPath.row], for: .normal)
        cell.enableShowBtn.layer.cornerRadius = 5
        cell.imgUploadByPatientBtn.addTarget(self, action: #selector(self.openPopUpVC), for: UIControl.Event.touchUpInside)

        return cell
    }
    
   @objc func openPopUpVC()
    {
//        let popvc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "AdvanceSettingPopUpVC") as! AdvanceSettingPopUpVC
//        self.addChild(popvc)
//        popvc.view.frame = self.view.frame
//        self.view.addSubview(popvc.view)
//        popvc.didMove(toParent: self)
        viewPopUp.isHidden = false
        tableView.alpha = 0.5
    }
    
    @IBAction func btnCross(_ sender: UIButton)
    {
        viewPopUp.isHidden = true
         tableView.alpha = 1
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        
    }
    
}
