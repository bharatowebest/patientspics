//
//  SettingVC.swift
//  PatientPics
//
//  Created by Ankit Bagda on 30/07/19.
//  Copyright © 2019 Om Prakash Jangid. All rights reserved.
//

import UIKit
import MobileCoreServices
import LGSideMenuController


class SettingVC: UIViewController,UIImagePickerControllerDelegate,UINavigationControllerDelegate {
    
    var picker:UIImagePickerController?=UIImagePickerController()

    
    @IBOutlet weak var customLogoTextField: UITextView!
    @IBOutlet weak var updateBtn: UIButton!
    @IBOutlet weak var cancleBtn: UIButton!
    @IBOutlet weak var customLogoImgView: UIImageView!
    @IBOutlet weak var brandingImgView: UIImageView!
    @IBOutlet weak var descriptionTxtField: UITextView!
    @IBOutlet weak var titleTxtField: UITextField!
    var sel = 0
   
    override func viewDidLoad() {
        super.viewDidLoad()

        titleTxtField.layer.borderWidth = 1
        descriptionTxtField.layer.borderWidth = 1
        titleTxtField.layer.borderColor = #colorLiteral(red: 0.8039215803, green: 0.8039215803, blue: 0.8039215803, alpha: 1)
        descriptionTxtField.layer.borderColor = #colorLiteral(red: 0.8039215803, green: 0.8039215803, blue: 0.8039215803, alpha: 1)
        customLogoImgView.layer.borderColor = #colorLiteral(red: 0.8039215803, green: 0.8039215803, blue: 0.8039215803, alpha: 1)
        brandingImgView.layer.borderColor = #colorLiteral(red: 0.8039215803, green: 0.8039215803, blue: 0.8039215803, alpha: 1)
        brandingImgView.layer.borderWidth = 1
        customLogoImgView.layer.borderWidth = 1
        cancleBtn.layer.cornerRadius = cancleBtn.frame.height / 2
        updateBtn.layer.cornerRadius = updateBtn.frame.height / 2
        
        cancleBtn.layer.borderColor = config.appThemeColor.cgColor
        updateBtn.layer.borderColor = config.appThemeColor.cgColor
        cancleBtn.layer.borderWidth = 1
        updateBtn.layer.borderWidth = 1
        
        navigationController?.navigationBar.barTintColor = config.appThemeColor

        
        var textView = UIView()
        textView = UIView(frame: CGRect(x: 0, y: 0, width: 20, height: 10))
        titleTxtField.addSubview(textView)
        
         let image = UIImage(named: "arrow-down20")
        let imageView = UIImageView(frame: CGRect(x:0, y: 0, width: 8  , height: 8))
        imageView.image = image
        textView.addSubview(imageView)
        titleTxtField.rightViewMode = UITextField.ViewMode.always
        titleTxtField.rightView = textView
        
        
        let tapGestureRecognizer1 = UITapGestureRecognizer(target: self, action: #selector(imageTapped1(tapGestureRecognizer:)))
        brandingImgView.isUserInteractionEnabled = true
        customLogoImgView.isUserInteractionEnabled = true
        picker?.delegate=self
        
        let tapGestureRecognizer2 = UITapGestureRecognizer(target: self, action: #selector(imageTapped2(tapGestureRecognizer:)))
        brandingImgView.isUserInteractionEnabled = true
        customLogoImgView.isUserInteractionEnabled = true
        picker?.delegate=self

        brandingImgView.addGestureRecognizer(tapGestureRecognizer1)
        customLogoImgView.addGestureRecognizer(tapGestureRecognizer2)

    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.navigationItem.leftBarButtonItem = nil
        self.navigationController?.navigationBar.isHidden = false
        
        let menuBarButtonItem = UIBarButtonItem(image: UIImage(named: "menu-button-2.png"), style: .plain, target: self, action: #selector(menuBtn))
        self.navigationItem.leftBarButtonItem  = menuBarButtonItem
        navigationController?.navigationBar.tintColor = .white
        
        var tabview = UIView()
        tabview.backgroundColor = UIColor.black
        tabview = UIView(frame: CGRect(x: 0, y: 0, width: 30, height: 40))
        let notificationBarButtonItem = UIButton(frame: CGRect(x: 10, y: 2, width: 30, height: 30))
        notificationBarButtonItem.setImage(UIImage(named: "bell20.png"), for: .normal)
        notificationBarButtonItem.addTarget(self, action: #selector(self.notificationBarButtonItemAction), for: UIControl.Event.touchUpInside)

        var notificationLabel = UILabel()
        notificationLabel = UILabel(frame: CGRect(x: 27, y: 4, width: 16, height: 16))
        notificationLabel.font = UIFont.boldSystemFont(ofSize: 12)
        notificationLabel.backgroundColor = UIColor.red
        notificationLabel.layer.cornerRadius = notificationLabel.frame.height / 2
        notificationLabel.layer.masksToBounds = true
        notificationLabel.text = "3"
        notificationLabel.textAlignment = .center
        notificationLabel.font = UIFont.boldSystemFont(ofSize: 16)
        notificationLabel.textColor = UIColor.white
        tabview.addSubview(notificationBarButtonItem)
        tabview.addSubview(notificationLabel)

        let rightBarButton = UIBarButtonItem(customView: tabview)
        self.navigationItem.rightBarButtonItem = rightBarButton
    }

    @objc func notificationBarButtonItemAction()
    {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "NotificationListVC") as! NotificationListVC
        navigationController?.pushViewController(vc, animated: true)
    }
    @objc func menuBtn()
    {
        sideMenuController?.showLeftViewAnimated()
    }
    
    @objc func imageTapped1(tapGestureRecognizer: UITapGestureRecognizer) {
        let tappedImage = tapGestureRecognizer.view as! UIImageView
        
        let alert:UIAlertController=UIAlertController(title: "Profile Picture Options", message: nil, preferredStyle: UIAlertController.Style.actionSheet)
        
        let gallaryAction = UIAlertAction(title: "Open Gallary", style: UIAlertAction.Style.default)
        {
            UIAlertAction in self.openGallary()
        }
        let CameraAction = UIAlertAction(title: "Open Camera", style: UIAlertAction.Style.default)
        {
            UIAlertAction in self.openCamera()
        }
        
        let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertAction.Style.cancel)
        {
            UIAlertAction in self.cancel()
            
        }
        
        alert.addAction(gallaryAction)
        alert.addAction(CameraAction)
        alert.addAction(cancelAction)
        sel = 0
        
        self.present(alert, animated: true, completion: nil)
        
    }
    
    
    @objc func imageTapped2(tapGestureRecognizer: UITapGestureRecognizer) {
        let tappedImage = tapGestureRecognizer.view as! UIImageView
        
        let alert:UIAlertController=UIAlertController(title: "Profile Picture Options", message: nil, preferredStyle: UIAlertController.Style.actionSheet)
        
        let gallaryAction = UIAlertAction(title: "Open Gallary", style: UIAlertAction.Style.default)
        {
            UIAlertAction in self.openGallary()
        }
        let CameraAction = UIAlertAction(title: "Open Camera", style: UIAlertAction.Style.default)
        {
            UIAlertAction in self.openCamera()
        }
        
        let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertAction.Style.cancel)
        {
            UIAlertAction in self.cancel()
            
        }
        
        alert.addAction(gallaryAction)
        alert.addAction(CameraAction)
        alert.addAction(cancelAction)
        
        sel = 1
        self.present(alert, animated: true, completion: nil)
        
    }
    
    func openGallary()
    {
        picker!.allowsEditing = false
        picker!.sourceType = UIImagePickerController.SourceType.photoLibrary
        present(picker!, animated: true, completion: nil)
    }
    
    func openCamera()
    {
        if UIImagePickerController.isSourceTypeAvailable(UIImagePickerController.SourceType.camera){
            print("Button capture")
            
            let imag = UIImagePickerController()
            imag.delegate = self
            imag.sourceType = UIImagePickerController.SourceType.camera;
            imag.mediaTypes = [kUTTypeImage] as [String]
            imag.allowsEditing = false
            
            self.present(imag, animated: true, completion: nil)
        }
    }
    
    func cancel(){
        print("Cancel Clicked")
    }
    
    private func imagePickerControllerDidCancel(picker: UIImagePickerController)
    {
        dismiss(animated: true, completion: nil)
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey: Any])
    {
        picker.dismiss(animated: true, completion: nil)
        let image = info[.originalImage] as! UIImage
        if sel == 0
        {
            brandingImgView.image = image
        }
        else
        {
            customLogoImgView.image = image
        }
    }
    
    @IBAction func cancle(_ sender: Any)
    {
        cancleBtn.setTitleColor(.white, for: .normal)
        cancleBtn.backgroundColor = config.appThemeColor
        updateBtn.titleLabel?.textColor = config.appThemeColor
        updateBtn.backgroundColor = UIColor.white
    }
    
    @IBAction func update(_ sender: Any)
    {
        updateBtn.titleLabel?.textColor = UIColor.white
        updateBtn.backgroundColor = config.appThemeColor
        cancleBtn.titleLabel?.textColor = config.appThemeColor
        cancleBtn.backgroundColor = UIColor.white
        
    }
   
    
}

