//
//  OfficeListCell.swift
//  PatientPics
//
//  Created by Ankit Bagda on 12/08/19.
//  Copyright © 2019 Om Prakash Jangid. All rights reserved.
//

import UIKit

class OfficeListCell: UITableViewCell {

    @IBOutlet weak var descriptionLbl: UILabel!
    @IBOutlet weak var nameLbl: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
