//
//  OfficeListVC.swift
//  PatientPics
//
//  Created by Ankit Bagda on 12/08/19.
//  Copyright © 2019 Om Prakash Jangid. All rights reserved.
//

import UIKit

class OfficeListVC: UIViewController,UITableViewDataSource, UITableViewDelegate {

    @IBOutlet weak var addOfficeLbl: UIButton!
    @IBOutlet weak var tableView: UITableView!
    
    var active = UIButton()
    var more = UIButton()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        addOfficeLbl.layer.cornerRadius = addOfficeLbl.frame.height / 2
        
    }
    override func viewWillAppear(_ animated: Bool) {
      
        self.title = "Setting"
        super.viewWillAppear(true)
        self.navigationItem.leftBarButtonItem = nil
        self.navigationController?.navigationBar.isHidden = false
        let menuBarButtonItem = UIBarButtonItem(image: UIImage(named:"menu-button-2.png"), style: .plain, target: self, action: #selector(menuBtn))
        self.navigationItem.leftBarButtonItem  = menuBarButtonItem
        navigationController?.navigationBar.tintColor = .white
        
        var tabview = UIView()
        tabview = UIView(frame: CGRect(x: 0, y: 0, width: 60, height: 40))
        
        let addBarButtonItem = UIButton(frame: CGRect(x: 16, y: 8, width: 16, height: 16))
        addBarButtonItem.setImage(UIImage(named: "add-W24.png"), for: .normal)
        addBarButtonItem.addTarget(self, action: #selector(self.addBarButtonItemAction), for: UIControl.Event.touchUpInside)
        
        let notificationBarButtonItem = UIButton(frame: CGRect(x: 40, y: 2, width: 30, height: 30))
        notificationBarButtonItem.setImage(UIImage(named: "bell20.png"), for: .normal)
        notificationBarButtonItem.addTarget(self, action: #selector(self.notificationBarButtonItemAction), for: UIControl.Event.touchUpInside)
        
        var notificationLabel = UILabel()
        notificationLabel = UILabel(frame: CGRect(x: 57, y: 4, width: 16, height: 16))
        notificationLabel.backgroundColor = UIColor.red
        notificationLabel.layer.cornerRadius = notificationLabel.frame.height / 2
        notificationLabel.layer.masksToBounds = true
        notificationLabel.text = "3"
        notificationLabel.textAlignment = .center
        notificationLabel.font = UIFont.boldSystemFont(ofSize: 16)
        notificationLabel.textColor = UIColor.white
        tabview.addSubview(notificationBarButtonItem)
        tabview.addSubview(notificationLabel)
        tabview.addSubview(addBarButtonItem)
        
        
        let rightBarButton = UIBarButtonItem(customView: tabview)
        self.navigationItem.rightBarButtonItem = rightBarButton
    }
    
    @objc func addBarButtonItemAction()
    {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "SettingAddProviderVC") as! SettingAddProviderVC
        vc.view.frame = self.view.bounds
        self.addChild(vc)
        self.view.addSubview(vc.view)
        vc.didMove(toParent: self)
    }
    @objc func notificationBarButtonItemAction()
    {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "NotificationListVC") as! NotificationListVC
        navigationController?.pushViewController(vc, animated: true)
    }
    @objc func menuBtn()
    {
        sideMenuController?.showLeftViewAnimated()
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 20
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "OfficeListCell", for: indexPath as IndexPath) as! OfficeListCell
        
        let activeBtn = UIButton(type: UIButton.ButtonType.custom) as UIButton
        activeBtn.backgroundColor = UIColor.white
        activeBtn.setTitleColor(UIColor.black, for: .normal)
        activeBtn.setTitle("Active", for: .normal)
        activeBtn.frame = CGRect(x: self.tableView.contentSize.width - 110, y: 5, width: 60, height: 30)
        activeBtn.titleLabel?.font = .systemFont(ofSize: 14)
        activeBtn.addTarget(self, action: #selector(self.activeAction(sender:)), for: .touchUpInside)
        activeBtn.tag = indexPath.row
        cell.contentView.addSubview(activeBtn)
      
        let moreBtn = UIButton(type: UIButton.ButtonType.custom) as UIButton
        moreBtn.backgroundColor = UIColor.white
        moreBtn.setImage(UIImage(named: "ellipsis20.png"), for: .normal)
        moreBtn.frame = CGRect(x: self.tableView.contentSize.width - 30, y: 5 , width: 20, height: 30)
        moreBtn.titleLabel?.font = .systemFont(ofSize: 14)
        moreBtn.addTarget(self, action: #selector(self.moreAction(sender:)), for: .touchUpInside)
        moreBtn.tag = indexPath.row
        cell.contentView.addSubview(moreBtn)
        
        let lineView = UIView() as UIView
        lineView.frame = CGRect(x: self.tableView.contentSize.width - 40, y: 0, width: 1, height: 52 )
        //lineView.backgroundColor = UIColor(displayP3Red: 238.0/255.0, green: 238.0/255.0, blue: 238.0/255.0, alpha: 1.0/25.0)
        lineView.backgroundColor = #colorLiteral(red: 0.9333333333, green: 0.9333333333, blue: 0.9333333333, alpha: 1)
        cell.contentView.addSubview(lineView)
        
        

        return cell
    }
    
    @objc func activeAction(sender: UIButton)
    {
        let buttonRow = sender.tag
        print("active is Pressed")
        print("Clicked active Row is",buttonRow)
      //  cell.textLabel.text = array[indexPath.row]
    }
    @objc func moreAction(sender: UIButton)
    {
        let buttonRow = sender.tag
        print("more is Pressed")
        print("Clicked more Row is",buttonRow)
        //  cell.textLabel.text = array[indexPath.row]
    }

    @IBAction func addOfficeAction(_ sender: Any)
    {
        let dashboard = self.storyboard?.instantiateViewController(withIdentifier: "SettingOfficeVC") as! SettingOfficeVC
        navigationController!.pushViewController(dashboard, animated: true)
    }
}
