//
//  SettingOfficeCell.swift
//  PatientPics
//
//  Created by Ankit Bagda on 01/08/19.
//  Copyright © 2019 Om Prakash Jangid. All rights reserved.
//

import UIKit

class SettingOfficeCell: UITableViewCell {

    @IBOutlet weak var downImgView: UIButton!
    @IBOutlet weak var addLineBtn: UIButton!
    @IBOutlet weak var textfileldAll: UITextField!
    @IBOutlet weak var checkBoxImgView: UIImageView!
    @IBOutlet weak var mailOfficeLbl: UILabel!
    @IBOutlet weak var namingLbl: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    override func draw(_ rect: CGRect) {
        textfileldAll.layer.borderWidth = 0.7
        textfileldAll.layer.borderColor = #colorLiteral(red: 0.7982209792, green: 0.8039124032, blue: 0.8209866751, alpha: 1)
    }

}
