//
//  SettingProviderListExpandVc.swift
//  PatientPics
//
//  Created by Ankit Bagda on 31/07/19.
//  Copyright © 2019 Om Prakash Jangid. All rights reserved.
//

import UIKit

class SettingOfficeVC: UIViewController,UITableViewDelegate,UITableViewDataSource,UITextViewDelegate,UIPickerViewDelegate,UIPickerViewDataSource
{
    //MARK:- Variables and Outlets
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var cancle: UIButton!
    @IBOutlet weak var saveBtn: UIButton!
    var pickerCountries: UIPickerView! = UIPickerView()
    var country = String()

    
    var nameLblArray = ["Office Title","Public Email","Public Phone Number","Fax","Website URL","Country","State","City","Zip Code","Street Address"]
    var textfieldNameArray = ["Type here","Type here","Type here","Type here","Type here","","Type here","Type here","Type here","Type here"]
    var CountryArray = ["USA", "India", "England","Nepal","UK"]

    override func viewDidLoad()
    {
        super.viewDidLoad()
        tableView.rowHeight = 70
        cancle.layer.cornerRadius = cancle.frame.height / 2
        cancle.layer.borderWidth = 1
        cancle.layer.borderColor = config.appThemeColor.cgColor
        cancle.titleLabel!.textColor = config.appThemeColor

        saveBtn.layer.cornerRadius = saveBtn.frame.height / 2
        saveBtn.layer.borderWidth = 1
        saveBtn.layer.borderColor = config.appThemeColor.cgColor
        saveBtn.titleLabel!.textColor = UIColor.white
        saveBtn.backgroundColor = config.appThemeColor
        
        }
    
        override func viewWillAppear(_ animated: Bool)
        {
            self.title = "Add Office"
        super.viewWillAppear(true)
        self.navigationItem.leftBarButtonItem = nil
        self.navigationController?.navigationBar.isHidden = false
        let backBarButtonItem = UIBarButtonItem(image: UIImage(named:"back-20w.png"), style: .plain, target: self, action: #selector(backBtn))
        self.navigationItem.leftBarButtonItem  = backBarButtonItem
        navigationController?.navigationBar.tintColor = .white
        
        var tabview = UIView()
        tabview = UIView(frame: CGRect(x: 0, y: 0, width: 60, height: 40))
        
        let notificationBarButtonItem = UIButton(frame: CGRect(x: 40, y: 2, width: 30, height: 30))
        notificationBarButtonItem.setImage(UIImage(named: "bell20.png"), for: .normal)
        notificationBarButtonItem.addTarget(self, action: #selector(self.notificationBarButtonItemAction), for: UIControl.Event.touchUpInside)
        
        var notificationLabel = UILabel()
        notificationLabel = UILabel(frame: CGRect(x: 57, y: 4, width: 16, height: 16))
        notificationLabel.font = notificationLabel.font.withSize(12.0)
        notificationLabel.backgroundColor = UIColor.red
        notificationLabel.layer.cornerRadius = notificationLabel.frame.height / 2
        notificationLabel.layer.masksToBounds = true
        notificationLabel.text = "3"
        notificationLabel.textAlignment = .center
            notificationLabel.font = UIFont.boldSystemFont(ofSize: 16)
        notificationLabel.textColor = UIColor.white
        tabview.addSubview(notificationBarButtonItem)
        tabview.addSubview(notificationLabel)
            
        let rightBarButton = UIBarButtonItem(customView: tabview)
        self.navigationItem.rightBarButtonItem = rightBarButton
    }
    
    @objc func addBarButtonItemAction()
    {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "SettingAddProviderVC") as! SettingAddProviderVC
        vc.view.frame = self.view.bounds
        self.addChild(vc)
        self.view.addSubview(vc.view)
        vc.didMove(toParent: self)
    }
    @objc func notificationBarButtonItemAction()
    {
     let vc = self.storyboard?.instantiateViewController(withIdentifier: "NotificationListVC") as! NotificationListVC
        navigationController?.pushViewController(vc, animated: true)
    }
    @objc func backBtn()
    {
        navigationController?.popViewController(animated: true)
    }
    
    //MARK:- PickerView Methods
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int{
        return CountryArray.count
    }
    
    private func pickerView(pickerView: UIPickerView!, titleForRow row: Int, forComponent component: Int) -> String! {
        return CountryArray[row]
    }
    
    private func pickerView(pickerView: UIPickerView!, didSelectRow row: Int, inComponent component: Int)
    {
        country = CountryArray[row]
        pickerCountries.isHidden = true;
    }
    private func textFieldShouldBeginEditing(textField: UITextField) -> Bool {
       pickerCountries.isHidden = false
        return false
    }
    
    //MARK:- TableView Methods
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return textfieldNameArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell = tableView.dequeueReusableCell(withIdentifier: "SettingOfficeCell", for: indexPath as IndexPath) as! SettingOfficeCell
        cell.textfileldAll.placeholder = textfieldNameArray[indexPath.row]
        cell.namingLbl.text = nameLblArray[indexPath.row]
        
        if indexPath.row == 0
        {
            cell.checkBoxImgView.image = UIImage(named: "checkbox20")
            cell.mailOfficeLbl.text = "This is the mail office"
        }
        else
        {
            cell.checkBoxImgView.image = UIImage(named: "")
            cell.mailOfficeLbl.text = ""
        }
        if indexPath.row == 9
        {
            cell.addLineBtn.setTitle("Add Line", for: .normal)
        }
        else
        {
            cell.addLineBtn.setTitle("", for: .normal)
        }
        if indexPath.row == 5 || indexPath.row == 6 || indexPath.row == 7
        {
            cell.downImgView.setImage(UIImage(named: "arrow-down20"), for: .normal)
        }
        else
        {
            cell.downImgView.setImage(UIImage(named: ""), for: .normal)
        }
        return cell
    }
    
    //MARK:- Button Actions
    @IBAction func saveBtn(_ sender: Any)
    {
        saveBtn.setTitleColor(#colorLiteral(red: 1, green: 1, blue: 1, alpha: 1), for: .normal)
        saveBtn.backgroundColor = config.appThemeColor
        cancle.setTitleColor(config.appThemeColor, for: .normal)
        cancle.backgroundColor = UIColor.white
    }
    
    @IBAction func cancleBtn(_ sender: Any)
    {
        cancle.setTitleColor(#colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0), for: .normal)
        cancle.backgroundColor = config.appThemeColor
        saveBtn.titleLabel?.textColor = config.appThemeColor
        saveBtn.backgroundColor = UIColor.white
    }

}
