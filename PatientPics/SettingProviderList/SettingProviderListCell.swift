//
//  SettingProviderListCell.swift
//  PatientPics
//
//  Created by Ankit Bagda on 31/07/19.
//  Copyright © 2019 Om Prakash Jangid. All rights reserved.
//

import UIKit

class SettingProviderListCell: UITableViewCell {

    @IBOutlet weak var moreCellBtn: UIButton!
    @IBOutlet weak var btnDelete: UIButton!
    @IBOutlet weak var menuImgView: UIImageView!
    @IBOutlet weak var cellExpandImageView: UIImageView!
    @IBOutlet weak var categoryLbl: UILabel!
    @IBOutlet weak var emailLbl: UILabel!
    @IBOutlet weak var nameLbl: UILabel!
    @IBOutlet weak var profileImgView: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
    
     
    
    }
    
    override func draw(_ rect: CGRect) {
        profileImgView.layer.cornerRadius = profileImgView.frame.height / 2
       
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
