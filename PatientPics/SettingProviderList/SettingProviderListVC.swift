
//
//  SettingProviderListVC.swift
//  PatientPics
//
//  Created by Ankit Bagda on 31/07/19.
//  Copyright © 2019 Om Prakash Jangid. All rights reserved.
//

import UIKit
import LGSideMenuController

struct tableStruct{
    var isOpend = Bool()
    var sectionTitle = String()
    var cellTitle = [String]()
}


class SettingProviderListVC: UIViewController,UITableViewDataSource,UITableViewDelegate {
   
    var tableArray = [tableStruct(isOpend: false, sectionTitle: "Title", cellTitle: ["","",""]),
                      tableStruct(isOpend: false, sectionTitle: "Title", cellTitle: ["","",""]),
                      tableStruct(isOpend: false, sectionTitle: "Title", cellTitle: ["","",""]),]
    

    var cellExpand = Bool()
    @IBOutlet weak var filterLbl: UILabel!
    @IBOutlet weak var filterBtn: UIButton!
    @IBOutlet weak var tableView: UITableView!
    
    @IBOutlet weak var filterView: UILabel!
    var showDelete = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        navigationController?.navigationBar.barTintColor = config.appThemeColor
        filterLbl.layer.cornerRadius = filterLbl.frame.height / 2
        filterLbl.layer.borderWidth = 0.7
        filterLbl.layer.borderColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
    }
    
    override func viewWillAppear(_ animated: Bool) {

        cellExpand = false
        
        super.viewWillAppear(true)
        self.navigationItem.leftBarButtonItem = nil
        self.navigationController?.navigationBar.isHidden = false
        let menuBarButtonItem = UIBarButtonItem(image: UIImage(named:"menu-button-2.png"), style: .plain, target: self, action: #selector(menuBtn))
        self.navigationItem.leftBarButtonItem  = menuBarButtonItem
        navigationController?.navigationBar.tintColor = .white
        
        var tabview = UIView()
        tabview = UIView(frame: CGRect(x: 0, y: 0, width: 60, height: 40))

        let addBarButtonItem = UIButton(frame: CGRect(x: 16, y: 8, width: 16, height: 16))
        addBarButtonItem.setImage(UIImage(named: "add20w.png"), for: .normal)
        addBarButtonItem.addTarget(self, action: #selector(self.addBarButtonItemAction), for: UIControl.Event.touchUpInside)

        let notificationBarButtonItem = UIButton(frame: CGRect(x: 40, y: 2, width: 30, height: 30))
        notificationBarButtonItem.setImage(UIImage(named: "bell20.png"), for: .normal)
        notificationBarButtonItem.addTarget(self, action: #selector(self.notificationBarButtonItemAction), for: UIControl.Event.touchUpInside)
        
        var notificationLabel = UILabel()
        notificationLabel = UILabel(frame: CGRect(x: 57, y: 4, width: 16, height: 16))
        notificationLabel.backgroundColor = UIColor.red
        notificationLabel.layer.cornerRadius = notificationLabel.frame.height / 2
        notificationLabel.layer.masksToBounds = true
        notificationLabel.text = "3"
        notificationLabel.textAlignment = .center
        notificationLabel.font = UIFont.boldSystemFont(ofSize: 16)
        notificationLabel.textColor = UIColor.white
        tabview.addSubview(notificationBarButtonItem)
        tabview.addSubview(notificationLabel)
        tabview.addSubview(addBarButtonItem)

        
        let rightBarButton = UIBarButtonItem(customView: tabview)
        self.navigationItem.rightBarButtonItem = rightBarButton
    }
    
   @objc func addBarButtonItemAction()
    {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "SettingAddProviderVC") as! SettingAddProviderVC
        vc.view.frame = self.view.bounds
        self.addChild(vc)
        self.view.addSubview(vc.view)
        vc.didMove(toParent: self)
    }
    
    @objc func notificationBarButtonItemAction()
    {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "NotificationListVC") as! NotificationListVC
        navigationController?.pushViewController(vc, animated: true)
    }
    
    @objc func menuBtn()
    {
    sideMenuController?.showLeftViewAnimated()
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
       
        return tableArray.count
    }
   
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {

        if tableArray[section].isOpend == true{
            return tableArray[section].cellTitle.count + 1
        } else
        {
            return 1
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
    
        if indexPath.row == 0{
            tableView.separatorStyle = .singleLine
            guard let cell = tableView.dequeueReusableCell(withIdentifier: "SettingProviderListCell", for: indexPath as IndexPath) as? SettingProviderListCell   else {
                return UITableViewCell()
            }
            if tableArray[indexPath.section].isOpend == true
            {
                if showDelete == 0
                {
                    cell.btnDelete.isHidden = true
                }
                else
                {
                    cell.btnDelete.isHidden = false
                }
                cell.moreCellBtn.tag = indexPath.section
                cell.moreCellBtn.setImage(UIImage(named: "ellipsis20"), for: .normal)
                cell.moreCellBtn.addTarget(self, action: #selector(moreBtnAction), for: .touchUpInside)
                cell.btnDelete.addTarget(self, action: #selector(deleteBtnAction), for: .touchUpInside)
                
                cell.cellExpandImageView.image = #imageLiteral(resourceName: "arrow-down20")
                
                return cell
            }
            else if tableArray[indexPath.section].isOpend == false
            {
                cell.btnDelete.isHidden = true
                cell.moreCellBtn.setImage(UIImage(named: ""), for: .normal)
                cell.cellExpandImageView.image = #imageLiteral(resourceName: "arrow-right-B20")
                tableView.tableFooterView = UIView()
                return cell
            }
            tableView.tableFooterView = UIView()
            return cell
        }else
        {
            tableView.separatorStyle = .none
            guard  let cell = tableView.dequeueReusableCell(withIdentifier: "SettingProviderListExpandCell", for: indexPath as IndexPath) as? SettingProviderListExpandCell  else {
                tableView.tableFooterView = UIView()

                return UITableViewCell()
            }
            cell.textLabel?.text = tableArray[indexPath.section].cellTitle[indexPath.row - 1]
            tableView.tableFooterView = UIView()

//          tableView.rowHeight = 30
            return cell
        }
        
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        if cellExpand == false
        {
            cellExpand = true
        }
        else if cellExpand == true
        {
            cellExpand = false
        }

        tableArray[indexPath.section].isOpend = !tableArray[indexPath.section].isOpend
        self.tableView.reloadSections(IndexSet([indexPath.section]), with: .automatic)
         }

    @objc func moreBtnAction(_ sender: UIButton)
    {
        print(sender.tag)
        print("more pressed")
        let indexpath = IndexPath(row: 0, section: sender.tag)
        let cell = tableView.cellForRow(at: indexpath) as! SettingProviderListCell
        
        if showDelete == 0
        {
            showDelete = 1
            cell.btnDelete.isHidden = true
        }
        else
        {
            showDelete = 0
            cell.btnDelete.isHidden = false
        }
//        tableView.reloadData()
    }
    
    @objc func deleteBtnAction()
    {
        print("delete pressed")
        showDelete = 0
        tableView.reloadData()
    }
    
    
}

