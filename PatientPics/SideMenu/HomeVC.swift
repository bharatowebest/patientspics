//  HomeVC.swift
//
//
//  Created by Ankit Bagda on 30/07/19.
//

import UIKit
import LGSideMenuController

class HomeVC: UIViewController {
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
    }
    
    @IBAction func Menu(_ sender: Any) {
        //sideMenuController?.leftViewWidth = self.view.frame.width - 130
        sideMenuController!.leftViewWidth = 270.0;
        
        //        sideMenuController?.leftViewPresentationStyle = .slideAbove
        sideMenuController?.showLeftViewAnimated()
        
    }
    
}
