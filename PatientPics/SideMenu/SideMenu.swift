//
//  SideMenu.swift
//  SideMenuBarWithLGS
//
//  Created by Ankit Bagda on 30/07/19.
//  Copyright © 2019 Om Prakash Jangid. All rights reserved.
//

import UIKit
import LGSideMenuController

//struct subSturct
//{
//    var title = String()
//    var isSelected = Bool()
//    init(title:String) {
//        self.title = title
//    }
//
//}

class SideMenu: UIViewController,UITableViewDelegate,UITableViewDataSource
{
    //    var subArry = [subSturct(title: "Information Settings"),subSturct(title: "Provider Permissions"),subSturct(title: "Offices"),subSturct(title: "Imaging Tool"),subSturct(title: "Advance Setting")]
    
    var tableArray = [
        tableStruct(isOpend: false, sectionTitle: "Title", cellTitle: [])
        ,tableStruct(isOpend: false, sectionTitle: "Title", cellTitle: ["Information Settings","Provider Permissions","Offices","Imaging Tool","Advance Settings"]),
         tableStruct(isOpend: false, sectionTitle: "Title", cellTitle: []),
         tableStruct(isOpend: false, sectionTitle: "Title", cellTitle: []),
         tableStruct(isOpend: false, sectionTitle: "Title", cellTitle: []),
         tableStruct(isOpend: false, sectionTitle: "Title", cellTitle: []),
         tableStruct(isOpend: false, sectionTitle: "Title", cellTitle: []),
         tableStruct(isOpend: false, sectionTitle: "Title", cellTitle: [])
    ]
    
    var selectedRow = -1
    
    var imgArr = [#imageLiteral(resourceName: "dashboard-B20"),#imageLiteral(resourceName: "setting-B20"),#imageLiteral(resourceName: "dashboarduser-B20"),#imageLiteral(resourceName: "dashboarduser-B20"),#imageLiteral(resourceName: "doc-B20"),#imageLiteral(resourceName: "content-B40") ,#imageLiteral(resourceName: "leagl-B20"),#imageLiteral(resourceName: "dashboarduser-B20")]
    var i = 0
    var cellExpand:Bool = false
    var whiteImgArray: [UIImage] = [UIImage(named: "dashboard-W20")!,UIImage(named: "setting-W20.png")!,UIImage(named: "dashboarduser-W20.png")!,
                                    UIImage(named: "dashboarduser-W20.png")!,UIImage(named: "doc-W20.png")!,UIImage(named: "content-W20")!,
                                    UIImage(named: "leagl-W20.png")!,UIImage(named: "dashboarduser-W20.png")!]
    var nameArr = ["Dashbord","Settings","Patients","Staff","Documents", "Content", "Legal","My Profile"]
    
    var isopen = 0
    
    @IBOutlet weak var tableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
    }
    
    override func viewWillAppear(_ animated: Bool)
    {
        super.viewWillAppear(true)
        self.navigationController?.setNavigationBarHidden(true, animated: true)
    }
    
    override func viewWillDisappear(_ animated: Bool)
    {
        super.viewWillDisappear(true)
        self.navigationController?.setNavigationBarHidden(false, animated: false)
    }
    
    @IBAction func logoutButtonPress(_ sender: Any) {
        let window = UIApplication.shared.keyWindow
        UserDefaults.standard.setLogin(false)
        self.view.makeToast(message: "Logged Out Successfully")
        let vc = LoginAndRegisterVC(nibName: "LoginAndRegisterVC", bundle: nil)
        window?.rootViewController = UINavigationController(rootViewController: vc)
        
    }
    func numberOfSections(in tableView: UITableView) -> Int
    {
        return tableArray.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if tableArray[section].isOpend == true{
            return tableArray[section].cellTitle.count + 1
        } else
        {
            return 1
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        
        print(indexPath.section)
        print(indexPath.row)
        
        if indexPath.row == 0{
            
            guard let cell = tableView.dequeueReusableCell(withIdentifier: "SideMenuCell", for: indexPath as IndexPath) as? SideMenuCell   else {
                return UITableViewCell()
            }
            
            if indexPath.section == 1
            {
                cell.btnExpand.isHidden = false
                cell.NameLbl.text = nameArr[indexPath.section]
                if tableArray[indexPath.section].isOpend == true
                {
                    cell.btnExpand.setImage(UIImage(named: "downArrowWhite"), for: .normal)
                    cell.contentView.backgroundColor = config.appThemeColor
                    cell.imgView.image = whiteImgArray[indexPath.section]
                    cell.NameLbl.textColor = UIColor.white
                    
                }
                else
                {
                    cell.btnExpand.setImage(UIImage(named: "arrow-right-B20"), for: .normal)
                    cell.contentView.backgroundColor = UIColor.white
                    cell.imgView.image = imgArr[indexPath.section]
                    cell.NameLbl.textColor = UIColor.black
                    
                }
                
                
                
                
            }
            else
            {
                cell.btnExpand.isHidden = true
                cell.imgView.image = imgArr[indexPath.section]
                cell.NameLbl.text = nameArr[indexPath.section]
                cell.imgView.highlightedImage = whiteImgArray[indexPath.section]
                if tableArray[indexPath.section].isOpend
                {
                    cell.NameLbl.isHighlighted = true
                    cell.imgView.isHighlighted = true
                    cell.NameLbl.textColor = UIColor.white
                    cell.contentView.backgroundColor = config.appThemeColor
                }
                else{
                    cell.NameLbl.isHighlighted = false
                    cell.imgView.isHighlighted = false
                    cell.NameLbl.textColor = UIColor.black
                    cell.contentView.backgroundColor = UIColor.white
                }
                
                
            }
            return cell
        }
        else
        {
            
            tableView.separatorStyle = .none
            guard  let cell = tableView.dequeueReusableCell(withIdentifier: "SettingExpandingCell", for: indexPath as IndexPath) as? SettingExpandingCell  else {
                return UITableViewCell()
            }
            cell.contantNameLbl?.text = tableArray[indexPath.section].cellTitle[indexPath.row - 1]
            cell.contantNameLbl.textColor = (selectedRow == indexPath.row) ? config.appThemeColor : UIColor.black
            tableView.tableFooterView = UIView()
            return cell
            // }
        }
    }
    func unselectdAllrows()
    {
        for i in tableArray.indices
        {
            tableArray[i].isOpend = false
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        let section = indexPath.section
        unselectdAllrows()
        tableArray[indexPath.section].isOpend = true
        tableView.reloadData()
        
        if section == 0
        {
            let push = self.storyboard?.instantiateViewController(withIdentifier: "DashboardVC") as! DashboardVC
            pushNewController(controller: push)
            selectedRow = -1
        }
        
        if section == 1
        {
            
            if indexPath.row > 0
            {
                self.selectedRow = indexPath.row
            }
            switch (indexPath.row)
            {
            case 1:
                
                let dashboard = self.storyboard?.instantiateViewController(withIdentifier: "SettingVC") as! SettingVC
                pushNewController(controller: dashboard)
            case 2:
                let dashboard = self.storyboard?.instantiateViewController(withIdentifier: "SettingProviderListVC") as! SettingProviderListVC
                pushNewController(controller: dashboard)
            case 3:
                let dashboard = self.storyboard?.instantiateViewController(withIdentifier: "OfficeListVC") as! OfficeListVC
                pushNewController(controller: dashboard)
            case 4:
                let dashboard = self.storyboard?.instantiateViewController(withIdentifier: "DemoVC") as! DemoVC
                pushNewController(controller: dashboard)
            case 5:
                let dashboard = self.storyboard?.instantiateViewController(withIdentifier: "AdvanceSettingVC") as! AdvanceSettingVC
                pushNewController(controller: dashboard)
            default:
                break
                
            }
            
            
            
        }
        else if section == 2
        {
            let dashboard = self.storyboard?.instantiateViewController(withIdentifier: "PatientsListVC") as! PatientsListVC
            pushNewController(controller: dashboard)
            selectedRow = -1
        }
        else if section == 3
        {
            let push = self.storyboard?.instantiateViewController(withIdentifier: "StaffVC") as! StaffVC
            pushNewController(controller: push)
            selectedRow = -1
        }
        else if section == 4
        {
            let push = self.storyboard?.instantiateViewController(withIdentifier: "DocumentVC") as! DocumentVC
            pushNewController(controller: push)
            selectedRow = -1
            
            
        }
        else if section == 5
        {
            let push = self.storyboard?.instantiateViewController(withIdentifier: "ContentVC") as! ContentVC
            pushNewController(controller: push)
            selectedRow = -1
        }
        else if section == 6
        {
            let push = self.storyboard?.instantiateViewController(withIdentifier: "PracticeLegalInfo") as! PracticeLegalInfo
            pushNewController(controller: push)
            selectedRow = -1
        }
        else if section == (nameArr.count - 1)
        {
            let push = userProfileRoot(nibName: "userProfileRoot", bundle: nil)
            pushNewController(controller: push)
            selectedRow = -1
        }
        
    }
    
    
    
    func pushNewController(controller:UIViewController)
    {
        if let rootVC = self.parent as? LGSideMenuController
        {
            rootVC.hideLeftView()
            for item in rootVC.children
            {
                if let nav = item as? UINavigationController
                {
                    nav.pushViewController(controller, animated: true)
                    
                }
            }
        }
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60
    }
}
