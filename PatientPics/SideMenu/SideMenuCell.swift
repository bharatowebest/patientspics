//
//  SideMenuCell.swift
//  SideMenuBarWithLGS
//
//  Created by Ankit Bagda on 30/07/19.
//  Copyright © 2019 Om Prakash Jangid. All rights reserved.
//

import UIKit

class SideMenuCell: UITableViewCell {
    
    @IBOutlet weak var NameLbl: UILabel!
    @IBOutlet weak var imgView: UIImageView!
    @IBOutlet weak var btnExpand: UIButton!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
}
