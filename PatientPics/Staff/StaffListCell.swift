//
//  StaffListCell.swift
//  PatientPics
//
//  Created by Ankit Bagda on 09/08/19.
//  Copyright © 2019 Om Prakash Jangid. All rights reserved.
//

import UIKit

class StaffListCell: UITableViewCell {

   
    @IBOutlet weak var cellMenuBtn: UIButton!
    @IBOutlet weak var lineViewShow: UIView!
    @IBOutlet weak var dobLbl: UILabel!
    @IBOutlet weak var emailLbl: UILabel!
    @IBOutlet weak var nameLbl: UILabel!
    @IBOutlet weak var smallView: UIView!
    @IBOutlet weak var profilemgvVew: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    override func draw(_ rect: CGRect) {
        profilemgvVew.layer.cornerRadius = profilemgvVew.frame.height / 2
    }

}
