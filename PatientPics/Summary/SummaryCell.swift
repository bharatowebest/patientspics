//
//  SummaryCell.swift
//  PatientPics
//
//  Created by Ankit Bagda on 03/08/19.
//  Copyright © 2019 Om Prakash Jangid. All rights reserved.
//

import UIKit

class SummaryCell: UITableViewCell {

    @IBOutlet weak var textFieldBiodata: UITextField!
    @IBOutlet weak var downArrowBtnInTextFld: UIButton!
    @IBOutlet weak var lblBiodata: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }

    override func draw(_ rect: CGRect) {
        textFieldBiodata.layer.borderWidth = 0.8
        textFieldBiodata.layer.borderColor = #colorLiteral(red: 0.7921568627, green: 0.8156862745, blue: 0.8588235294, alpha: 1)
    }
}
