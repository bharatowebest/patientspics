//
//  SummaryVC.swift
//  PatientPics
//
//  Created by Ankit Bagda on 03/08/19.
//  Copyright © 2019 Om Prakash Jangid. All rights reserved.
//

import UIKit


class SummaryVC: UIViewController,UITableViewDelegate,UITableViewDataSource {

    @IBOutlet weak var profileEditButton: UIButton!
    @IBOutlet weak var profileImgView: UIImageView!
    @IBOutlet weak var editButton: UIButton!
    @IBOutlet weak var menuBtn: UIButton!
    @IBOutlet weak var userNameLbl: UILabel!
    @IBOutlet weak var tableView: UITableView!
    
    var lblBiodataArray = ["First Name", "Last Name", "Date Of Birth", "Email", "Gender"]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        profileImgView.layer.cornerRadius = profileImgView.frame.height / 2
        profileEditButton.layer.cornerRadius = profileEditButton.frame.height / 2
        tableView.rowHeight = 80
        

    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return 5
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell = tableView.dequeueReusableCell(withIdentifier: "SummaryCell", for: indexPath as IndexPath) as? SummaryCell
        cell?.lblBiodata.text = lblBiodataArray[indexPath.row]
        
//        cell?.nameTxtField.attributedPlaceholder = NSAttributedString(string: textFieldArr[indexPath.row] ,
//                                                                      attributes: [NSAttributedString.Key.foregroundColor: UIColor(displayP3Red: 229.0/255.0, green: 229.0/255.0, blue: 229.0/255.0, alpha: 1.0)])
//
        if indexPath.row == 4
        {
            cell?.downArrowBtnInTextFld.isHidden = false

            cell?.downArrowBtnInTextFld.setImage(UIImage(named: "arrow-down20.png"), for: .normal)
            cell?.downArrowBtnInTextFld.tintColor = UIColor.black
        }else
        {
            cell?.downArrowBtnInTextFld.isHidden = true
          //  cell?.downArrowBtnInTextFld.setImage(UIImage(named: ""), for: .normal)

        }
        
        return cell!
    }
}
