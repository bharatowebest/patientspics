//
//  rootViewController.swift
//  PatientPics
//
//  Created by Ankit Bagda on 03/08/19.
//  Copyright © 2019 Om Prakash Jangid. All rights reserved.
//

import UIKit
import CarbonKit

class rootViewController: UIViewController,CarbonTabSwipeNavigationDelegate {

    let items = ["Summary", "TimeLine", "Email", "Files"]

    override func viewDidLoad() {
        super.viewDidLoad()
        
        let carbonTabSwipeNavigation = CarbonTabSwipeNavigation(items: items, delegate: self)
        carbonTabSwipeNavigation.insert(intoRootViewController: self)
        //  carbonTabSwipeNavigation.insertIntoRootViewController(self, andTargetView: "SummaryVC")
    }
    override func viewWillAppear(_ animated: Bool) {
        
        self.navigationItem.leftBarButtonItem = nil
        self.navigationController?.navigationBar.isHidden = false
        self.title = "Patients"
        let menuBarButtonItem = UIBarButtonItem(image: UIImage(named: "back-20w.png"), style: .plain, target: self, action: #selector(menuBtn))
        self.navigationItem.leftBarButtonItem  = menuBarButtonItem
        
        var tabview = UIView()
        tabview = UIView(frame: CGRect(x: 0, y: 0, width: 60, height: 30))
        
       
        let notificationBarButtonItem = UIButton(frame: CGRect(x: 40, y: 0, width: 30, height: 30))
        notificationBarButtonItem.setImage(UIImage(named: "bell20.png"), for: .normal)
        notificationBarButtonItem.addTarget(self, action: #selector(self.notificationBarButtonItemAction), for: UIControl.Event.touchUpInside)
        
        var notificationLabel = UILabel()
        notificationLabel = UILabel(frame: CGRect(x: 57, y: 4, width: 10, height: 10))
        notificationLabel.backgroundColor = UIColor.red
        notificationLabel.layer.cornerRadius = notificationLabel.frame.height / 2
        notificationLabel.layer.masksToBounds = true
        notificationLabel.text = "3"
        notificationLabel.font = UIFont.boldSystemFont(ofSize: 16)
        notificationLabel.textAlignment = .center
        notificationLabel.textColor = UIColor.white
        let searchBarButtonItem = UIButton(frame: CGRect(x: 0, y: 0, width: 30, height: 30))
        searchBarButtonItem.setImage(UIImage(named: "search20.png"), for: .normal)
        searchBarButtonItem.addTarget(self, action: #selector(self.searchBarButtonItemAction), for: UIControl.Event.touchUpInside)
        navigationController?.navigationBar.tintColor = .white
        
        tabview.addSubview(notificationBarButtonItem)
        tabview.addSubview(notificationLabel)
        tabview.addSubview(searchBarButtonItem)
        
        let rightBarButton = UIBarButtonItem(customView: tabview)
        self.navigationItem.rightBarButtonItem = rightBarButton
    }
    
    @objc func menuBtn()
    {
    self.navigationController?.popViewController(animated: true)
    }
    
    @objc func  notificationBarButtonItemAction()
    {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "NotificationListVC") as! NotificationListVC
        navigationController?.pushViewController(vc, animated: true)
    }
    
    @objc func  addBarButtonItemAction()
    {
        
    }
    
    @objc func  searchBarButtonItemAction()
    {
        
    }
    
    func carbonTabSwipeNavigation(_ carbonTabSwipeNavigation: CarbonTabSwipeNavigation, viewControllerAt index: UInt) -> UIViewController {
        
        if index == 0
        {
            let screen1 = self.storyboard?.instantiateViewController(withIdentifier: "SummaryVC") as! SummaryVC
            return screen1

        }else if index == 1
        {
            let screen2 = self.storyboard?.instantiateViewController(withIdentifier: "TimelineVC") as! TimelineVC
            return screen2
        }else if index == 2
        {
            let screen3 = self.storyboard?.instantiateViewController(withIdentifier: "EmailVC") as! EmailVC
            return screen3
        }else if index == 3
        {
            let screen4 = self.storyboard?.instantiateViewController(withIdentifier: "FilesVC") as! FilesVC
            return screen4
        }
        return UIViewController()
    }
}
