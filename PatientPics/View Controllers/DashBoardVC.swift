//
//  DashBoardVC.swift
//  PatientPics
//
//  Created by apple on 7/29/19.
//  Copyright © 2019 apple. All rights reserved.
//

import UIKit

class DashBoardVC: UIViewController, UITableViewDelegate, UITableViewDataSource
{
    @IBOutlet weak var tblView: UITableView!
    var arrPatients = ["John Doe", "Harry Lobbert", "Tom Here", "Abel Adem", "Camille Abraham"]
    var arrDoctors = ["John Doe", "Harry Lobbert", "Tom Here", "Abel Adem", "Camille Abraham"]
    var arrColor = ["orange", "red", "cyan", "pink"]
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
    }
    
    //MARK: - TableView Delegates and Datasourcesw
    func numberOfSections(in tableView: UITableView) -> Int
    {
        return 3
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView?
    {
        
        print(section)
        let headerFrame = tableView.frame
        let headerView:UIView = UIView(frame: CGRect(x: 0, y: 0, width: headerFrame.size.width, height: headerFrame.size.height))
        
        headerView.backgroundColor = UIColor.white
        if section == 0
        {
            let lblHeading = UILabel()
            lblHeading.frame = CGRect(x: 8, y: 0, width: 250, height: 40)
            lblHeading.text = "Welcome to your Dashboard!"
            
            
            headerView.addSubview(lblHeading)
            
            return headerView
        }
        else if section == 1
        {
            let lblHeading = UILabel()
            lblHeading.frame = CGRect(x: 8, y: 16, width: 140, height: 24)
            lblHeading.text = "Recent Patients"
            lblHeading.textColor = UIColor(red: 0/255, green: 188/255, blue: 184/255, alpha: 1.0)
            lblHeading.font = UIFont.boldSystemFont(ofSize: 17.0)
            
            let btnPeriod = UIButton()
            btnPeriod.frame = CGRect(x: lblHeading.frame.origin.x + lblHeading.frame.width + 110, y: 16, width: 130, height: 24)
            btnPeriod.setTitleColor(UIColor.black, for: .normal)
            btnPeriod.setTitle("  Period: Day       v   ", for: .normal)
            btnPeriod.layer.borderWidth = 1
            btnPeriod.layer.borderColor = UIColor.black.cgColor
            btnPeriod.layer.cornerRadius = btnPeriod.frame.height / 2
            btnPeriod.titleLabel?.font = UIFont(name: "Helvetica neue", size: 14.0)
            headerView.addSubview(lblHeading)
            headerView.addSubview(btnPeriod)
            return headerView
        }
        else
        {
            let lblHeading = UILabel()
            lblHeading.frame = CGRect(x: 8, y: 16, width: 140, height: 24)
            lblHeading.text = "Recent Doctors"
            lblHeading.font = UIFont.boldSystemFont(ofSize: 17.0)
            lblHeading.textColor = UIColor(red: 0/255, green: 188/255, blue: 184/255, alpha: 1.0)
            let btnPeriod = UIButton()
            btnPeriod.frame = CGRect(x: lblHeading.frame.origin.x + lblHeading.frame.width + 110, y: 16, width: 130, height: 24)
            btnPeriod.setTitleColor(UIColor.black, for: .normal)
            btnPeriod.setTitle("  Period: Day       v   ", for: .normal)
            btnPeriod.layer.borderWidth = 1
            btnPeriod.layer.borderColor = UIColor.black.cgColor
            btnPeriod.layer.cornerRadius = btnPeriod.frame.height / 2
            btnPeriod.titleLabel?.font = UIFont(name: "Helvetica neue", size: 14.0)
            headerView.addSubview(lblHeading)
            headerView.addSubview(btnPeriod)
            return headerView
        }
        
        
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat
    {
        if section == 0
        {
            return 50
        }
        else if section == 1
        {
            return 50
        }
        else
        {
            return 50
        }
        
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        if section == 0
        {
            return 1
        }
        else if section == 1
        {
            return arrPatients.count
        }
        else
        {
            return arrDoctors.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        //MARK: - Cell Total Patients
        if indexPath.section == 0
        {
            let cellTotal = tblView.dequeueReusableCell(withIdentifier: "TotalPatientsCell") as! TotalPatientsCell
            
            return cellTotal
        }
        //MARK: - Cell Patients
        else if indexPath.section == 1
        {
            let cellPatients = tblView.dequeueReusableCell(withIdentifier: "RecentPatientsCell") as! RecentPatientsCell
            cellPatients.lblNameInitial.text = arrPatients[indexPath.row]
            cellPatients.lblName.text = arrPatients[indexPath.row]
            return cellPatients
        }
        //MARK: - Cell Doctor
        else
        {
            let cellDoctor = tblView.dequeueReusableCell(withIdentifier: "RecentDoctorsCell") as! RecentDoctorsCell
            cellDoctor.lblName.text = arrPatients[indexPath.row]
            return cellDoctor
        }
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        if indexPath.section == 0
        {
            return 200
        }
        else if indexPath.section == 1
        {
            return 105
        }
        else
        {
            return 105
        }
        
    }

}
