//
//  SignInVC.swift
//  PatientPics
//
//  Created by apple on 7/26/19.
//  Copyright © 2019 apple. All rights reserved.
//

import UIKit

class SignInVC: UIViewController
{
    //MARK:- Outlets
    @IBOutlet weak var btnLogin: UIButton!
    @IBOutlet weak var btnLoginStack: UIButton!
    @IBOutlet weak var btnRegister: UIButton!
    @IBOutlet weak var txtEmail: UITextField!
    @IBOutlet weak var txtPassword: UITextField!
    
    //MARK:- Variables
    
    

    override func viewDidLoad()
    {
        super.viewDidLoad()
        btnLogin.layer.cornerRadius = 17
        
        
    }
    
    //MARK:- Actions
    
    @IBAction func btnRegisterAction(_ sender: Any)
    {
        
    }
    
    @IBAction func btnLoginStackAction(_ sender: Any)
    {
        
    }
    
    

}
