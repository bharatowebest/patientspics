//
//  SelectImageVC.swift
//  PatientPics
//
//  Created by apple on 8/19/19.
//  Copyright © 2019 Om Prakash Jangid. All rights reserved.
//

import UIKit
import iOSPhotoEditor
import AVFoundation
import CoreMotion
import AADraggableView

class SelectImageVC: UIViewController, UIImagePickerControllerDelegate, UINavigationControllerDelegate, PhotoEditorDelegate, AVCapturePhotoCaptureDelegate
{
    @IBOutlet weak var imgView: UIImageView!
    @IBOutlet weak var TiltImgView: UIImageView!
    @IBOutlet weak var stackView: UIStackView!
    @IBOutlet weak var previewView: UIView!
    @IBOutlet weak var btnCapture: UIButton!
    @IBOutlet weak var btnCancel: UIButton!
    @IBOutlet weak var btnCancelStack: UIButton!
    @IBOutlet weak var btnGhostStack: UIButton!
    @IBOutlet weak var btnEditStack: UIButton!
    
    var picker = UIImagePickerController()
    var chosenImage = UIImage()
    var ghost = 0
    var session: AVCaptureSession?
    var stillImageOutput: AVCapturePhotoOutput?
    var videoPreviewLayer: AVCaptureVideoPreviewLayer?
    let backCamera =  AVCaptureDevice.default(for: AVMediaType.video)
    var input: AVCaptureDeviceInput!
    var error: NSError?
    var currentlyEditingView:RKUserResizableView? = nil
    var lastEditedView:RKUserResizableView? = nil
    let lineView = CGRect(x: 50, y: 50, width: 200, height: 1)
    var userResizableView = RKUserResizableView()
    let rectView = CGRect(x: 50, y: 200, width: 200, height: 100)
    var imageResizableView = RKUserResizableView()
    let manager = CMMotionManager()
    var imageData : Data?
    var vw = AADraggableView()
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        vw.frame = CGRect(x: 50, y: 100, width: 200, height: 200)
        vw.backgroundColor = UIColor.red
        self.view.bringSubviewToFront(vw)
//        self.view.addSubview(vw)
        
        TiltImgView.isHidden = true
        if manager.isDeviceMotionAvailable {
            manager.deviceMotionUpdateInterval = 0.01
            manager.startDeviceMotionUpdates(to: OperationQueue.main) {
                (data, error) in
                if let data = data {
                    let rotation = atan2(data.gravity.x, data.gravity.y) - Double.pi
                    self.TiltImgView.transform = CGAffineTransform(rotationAngle: CGFloat(rotation))
//                    print(rotation)
                    
                }
            }
        }
    
        self.title = "Choose Image"
        btnCancel.layer.borderColor = UIColor.black.cgColor
        btnCancel.layer.borderWidth = 1
        btnCapture.layer.borderWidth = 1
        btnCapture.layer.borderColor = UIColor.black.cgColor
        btnCapture.layer.cornerRadius = 25
        btnCancel.isHidden = true
        btnCapture.isHidden = true
        
        self.navigationItem.leftBarButtonItem = nil
        self.navigationController?.navigationBar.isHidden = false
        let backBarButtonItem = UIBarButtonItem(image: UIImage(named:"menu-button-2.png"), style: .plain, target: self, action: #selector(backBtn))
        self.navigationItem.leftBarButtonItem  = backBarButtonItem
        navigationController?.navigationBar.tintColor = .white
        
        var tabview = UIView()
        tabview = UIView(frame: CGRect(x: 0, y: 0, width: 60, height: 40))
        
        let notificationBarButtonItem = UIButton(frame: CGRect(x: 40, y: 0, width: 36, height: 36))
        notificationBarButtonItem.setImage(UIImage(named: "bell20.png"), for: .normal)
        notificationBarButtonItem.addTarget(self, action: #selector(self.notificationBarButtonItemAction), for: UIControl.Event.touchUpInside)
        
        var notificationLabel = UILabel()
        notificationLabel = UILabel(frame: CGRect(x: 57, y: 2, width: 16, height: 16))
        notificationLabel.backgroundColor = UIColor.red
        notificationLabel.layer.cornerRadius = notificationLabel.frame.height / 2
        notificationLabel.layer.masksToBounds = true
        notificationLabel.text = "3"
        notificationLabel.textAlignment = .center
        notificationLabel.font = UIFont.boldSystemFont(ofSize: 16)
        notificationLabel.textColor = UIColor.white
        tabview.addSubview(notificationBarButtonItem)
        tabview.addSubview(notificationLabel)
        
        let rightBarButton = UIBarButtonItem(customView: tabview)
        self.navigationItem.rightBarButtonItem = rightBarButton
        
        let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(imageTapped(tapGestureRecognizer:)))
        imgView.isUserInteractionEnabled = true
        imgView.addGestureRecognizer(tapGestureRecognizer)
        picker.delegate = self
        stackView.isHidden = true
        previewView.isHidden = true
        
        btnCancelStack.layer.borderWidth = 1
        btnCancelStack.layer.borderColor = UIColor.black.cgColor
        btnCancelStack.layer.cornerRadius = 10
        btnCancelStack.clipsToBounds = true
        
        btnGhostStack.layer.borderWidth = 1
        btnGhostStack.layer.borderColor = UIColor.black.cgColor
        btnGhostStack.layer.cornerRadius = 10
        btnGhostStack.clipsToBounds = true
        
        btnEditStack.layer.borderWidth = 1
        btnEditStack.layer.borderColor = UIColor.black.cgColor
        btnEditStack.layer.cornerRadius = 10
        btnEditStack.clipsToBounds = true
        
        
        
        // (1) Create a user resizable view with a simple red background content view.
        // create Line
        
        userResizableView = RKUserResizableView(frame: lineView)
        userResizableView.delegate = self
        let contentView = UIView(frame: lineView)
        contentView.backgroundColor = UIColor.white
        userResizableView.contentView = contentView
        userResizableView.showEditingHandles()
        userResizableView.isPreventsPositionOutsideSuperview = true
        currentlyEditingView = userResizableView
        lastEditedView = userResizableView
//        self.imgView.addSubview(userResizableView)
        
        // (2) Create a second resizable view with a UIImageView as the content.
        // create Rectangle
        
        imageResizableView = RKUserResizableView(frame: rectView)
        let imageView = UIView(frame: rectView)
        imageResizableView.contentView = imageView
        imageResizableView.delegate = self
//        self.view.addSubview(imageResizableView)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        if imgView.image == UIImage(named: "photo")
        {
            imgView.contentMode = .scaleAspectFit
        }
        else
        {
//            imgView.contentMode = .scaleAspectFill
        }
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
//        videoPreviewLayer!.frame = previewView.bounds
//        videoPreviewLayer!.videoGravity = AVLayerVideoGravity.resizeAspectFill
    }
    
    
    @objc func backBtn()
    {
        sideMenuController?.showLeftViewAnimated()
    }
    
    @objc func notificationBarButtonItemAction()
    {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "NotificationListVC") as! NotificationListVC
        navigationController?.pushViewController(vc, animated: true)
    }
    
    @objc func imageTapped(tapGestureRecognizer: UITapGestureRecognizer)
    {
        if imgView.image == UIImage(named: "photo")
        {
            let alert:UIAlertController=UIAlertController(title: "Choose Image", message: nil, preferredStyle: UIAlertController.Style.actionSheet)
            
            let gallaryAction = UIAlertAction(title: "Open Gallery", style: UIAlertAction.Style.default)
            {
                UIAlertAction in self.openGallary()
            }
            let CameraAction = UIAlertAction(title: "Open Camera", style: UIAlertAction.Style.default)
            {
                UIAlertAction in self.openCamera()
            }
            
            let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertAction.Style.cancel)
            {
                UIAlertAction in self.cancel()
                
            }
            
            alert.addAction(gallaryAction)
            alert.addAction(CameraAction)
            alert.addAction(cancelAction)
            
            self.present(alert, animated: true, completion: nil)
            
            
        }
        else
        {
            
            if stackView.isHidden == true
            {
                stackView.isHidden = false
            }
            else
            {
                stackView.isHidden = true
            }
        }
        
    }
    
    func openGallary()
    {
        picker.allowsEditing = false
        picker.sourceType = UIImagePickerController.SourceType.photoLibrary
        present(picker, animated: true, completion: nil)
    }
    
    func openCamera()
    {
        if UIImagePickerController.isSourceTypeAvailable(UIImagePickerController.SourceType.camera){
            print("Button capture")
            
            let imag = UIImagePickerController()
            imag.delegate = self
            imag.sourceType = UIImagePickerController.SourceType.camera;
            imag.allowsEditing = false
            
            self.present(imag, animated: true, completion: nil)
        }
    }
    
    func cancel(){
        print("Cancel Clicked")
    }
    
    func imgGhosting()
    {
        imgView.alpha = 0.3
        
        do {
            input = try AVCaptureDeviceInput(device: backCamera!)
        } catch let error1 as NSError {
            error = error1
            input = nil
            print(error!.localizedDescription)
        }
        
        session = AVCaptureSession()
        session!.sessionPreset = AVCaptureSession.Preset.photo
        
        
        
        if error == nil && session!.canAddInput(input) {
            session!.addInput(input)
            stillImageOutput = AVCapturePhotoOutput()
            //            stillImageOutput?.outputSettings = [AVVideoCodecKey:  AVVideoCodecJPEG]
            if session!.canAddOutput(stillImageOutput!) {
                session!.addOutput(stillImageOutput!)
                videoPreviewLayer = AVCaptureVideoPreviewLayer(session: session!)
                videoPreviewLayer!.videoGravity = AVLayerVideoGravity.resizeAspect
                videoPreviewLayer!.frame = previewView.bounds
                videoPreviewLayer!.connection?.videoOrientation = AVCaptureVideoOrientation.portrait
                previewView.layer.addSublayer(videoPreviewLayer!)
                session!.startRunning()
            }
        }
//        session!.startRunning()
        stackView.isHidden = true
    }
    
    @IBAction func didTapOnTakePhotoButton(_ sender: UIButton)
    {
        let settings = AVCapturePhotoSettings()
        
        let previewPixelType = settings.availablePreviewPhotoPixelFormatTypes.first!
        let previewFormat = [kCVPixelBufferPixelFormatTypeKey as String: previewPixelType,
                             kCVPixelBufferWidthKey as String: 160,
                             kCVPixelBufferHeightKey as String: 160,
        ]
        settings.previewPhotoFormat = previewFormat
        stillImageOutput!.capturePhoto(with: settings, delegate: self)
//        previewImageView.isHidden = false
    }
    
    func photoOutput(_ captureOutput: AVCapturePhotoOutput, didFinishProcessingPhoto photoSampleBuffer: CMSampleBuffer?, previewPhoto previewPhotoSampleBuffer: CMSampleBuffer?, resolvedSettings: AVCaptureResolvedPhotoSettings, bracketSettings: AVCaptureBracketedStillImageSettings?, error: Error?)
    {
        print("hello")
        guard error == nil,
            let photoSampleBuffer = photoSampleBuffer else
        {
            print("Error capturing photo: \(String(describing: error))")
            return
        }
    }
    
    @available(iOS 11.0, *)
    
    func photoOutput(_ output: AVCapturePhotoOutput, didFinishProcessingPhoto photo: AVCapturePhoto, error: Error?)
    {
        imageData = photo.fileDataRepresentation()
        let settings = AVCapturePhotoSettings()
        settings.livePhotoVideoCodecType = .jpeg
        settings.flashMode = .auto
        let capturedImage = UIImage.init(data: imageData! , scale: 1.0)
//        previewImageView.image = capturedImage
        imgView.image = capturedImage
        if let image = capturedImage
        {
            UIImageWriteToSavedPhotosAlbum(image, nil, nil, nil)
        }
    }
    
    private func imagePickerControllerDidCancel(picker: UIImagePickerController)
    {
        dismiss(animated: true, completion: nil)
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any])
    {
        chosenImage = info[UIImagePickerController.InfoKey.originalImage] as! UIImage
        imgView.image = chosenImage
//        imgView.contentMode = .scaleAspectFit
        imgView.image = chosenImage
        stackView.isHidden = false
        if ghost == 1
        {
            imgView.alpha = 0.4
        }
        dismiss(animated:true, completion: nil)
    }
    
    @IBAction func btnCancel(_ sender: UIButton)
    {
        stackView.isHidden = true
        imgView.image = UIImage(named: "photo")
//        imgView.contentMode = .scaleAspectFit
        ghost = 0
    }
    
    @IBAction func btnNext(_ sender: UIButton)
    {
        let photoEditor = PhotoEditorViewController(nibName:"PhotoEditorViewController",bundle: Bundle(for: PhotoEditorViewController.self))
        photoEditor.photoEditorDelegate = self
        photoEditor.image = chosenImage
        for i in 0...6 {
            photoEditor.stickers.append(UIImage(named: i.description )!)
        }
        present(photoEditor, animated: true, completion: nil)
    }
    
    @IBAction func btnGhosting(_ sender: UIButton)
    {
        stackView.isHidden = true
        previewView.isHidden = false
        btnCapture.isHidden = false
        btnCancel.isHidden = false
        TiltImgView.isHidden = false
        self.imgGhosting()
        imgView.alpha = 0.4
        ghost = 1
        
//        imgView.contentMode = .scaleAspectFit
    }
    
    @IBAction func btnCancelCamera(_ sender: UIButton)
    {
        stackView.isHidden = false
        previewView.isHidden = true
        btnCapture.isHidden = true
        btnCancel.isHidden = true
        TiltImgView.isHidden = true
        imgView.alpha = 1
        ghost = 0
        session?.stopRunning()
    }
    
    func doneEditing(image: UIImage)
    {
        imgView.image = image
    }
    
    func canceledEditing()
    {
        print("Canceled")
    }

}


extension SelectImageVC:RKUserResizableViewDelegate {
    func userResizableViewDidBeginEditing(_ userResizableView: RKUserResizableView) {
        currentlyEditingView?.hideEditingHandles()
        currentlyEditingView = userResizableView
    }
    
    func userResizableViewDidEndEditing(_ userResizableView: RKUserResizableView) {
        lastEditedView = userResizableView
    }
}

extension SelectImageVC:UIGestureRecognizerDelegate {
    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldReceive touch: UITouch) -> Bool {
        if ((currentlyEditingView?.hitTest(touch.location(in: currentlyEditingView), with: nil)) != nil) {
            return false
        }
        return true
    }
    
    func hideEditingHandles() {
        // We only want the gesture recognizer to end the editing session on the last
        // edited view. We wouldn't want to dismiss an editing session in progress.
        lastEditedView?.hideEditingHandles()
    }
}
