//
//  LoginAndSignUpModal.swift
//  PatientPics
//
//  Created by jai kalra on 13/08/19.
//  Copyright © 2019 Om Prakash Jangid. All rights reserved.
//

import Foundation
import SwiftyJSON


class loginModal : NSObject, NSCoding{
    
    var data : loginData!
    var error : Bool!
    var message : String!
    var userId : Int!
    
    /**
     * Instantiate the instance using the passed json values to set the properties values
     */
    init(fromJson json: JSON!){
        if json.isEmpty{
            return
        }
        let dataJson = json["data"]
        if !dataJson.isEmpty{
            data = loginData(fromJson: dataJson)
        }
        error = json["error"].boolValue
        message = json["message"].stringValue
        userId = json["user_id"].intValue
    }
    
    /**
     * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
     */
    func toDictionary() -> [String:Any]
    {
        var dictionary = [String:Any]()
        if data != nil{
            dictionary["data"] = data.toDictionary()
        }
        if error != nil{
            dictionary["error"] = error
        }
        if message != nil{
            dictionary["message"] = message
        }
        if userId != nil{
            dictionary["user_id"] = userId
        }
        return dictionary
    }
    
    /**
     * NSCoding required initializer.
     * Fills the data from the passed decoder
     */
    @objc required init(coder aDecoder: NSCoder)
    {
        data = aDecoder.decodeObject(forKey: "data") as? loginData
        error = aDecoder.decodeObject(forKey: "error") as? Bool
        message = aDecoder.decodeObject(forKey: "message") as? String
        userId = aDecoder.decodeObject(forKey: "user_id") as? Int
    }
    
    /**
     * NSCoding required method.
     * Encodes mode properties into the decoder
     */
    func encode(with aCoder: NSCoder)
    {
        if data != nil{
            aCoder.encode(data, forKey: "data")
        }
        if error != nil{
            aCoder.encode(error, forKey: "error")
        }
        if message != nil{
            aCoder.encode(message, forKey: "message")
        }
        if userId != nil{
            aCoder.encode(userId, forKey: "user_id")
        }
        
    }
    
}


class loginData : NSObject, NSCoding{
    
    var token : String!
    var userId : Int!
    
    /**
     * Instantiate the instance using the passed json values to set the properties values
     */
    init(fromJson json: JSON!){
        if json.isEmpty{
            return
        }
        token = json["token"].stringValue
        userId = json["user_id"].intValue
    }
    
    /**
     * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
     */
    func toDictionary() -> [String:Any]
    {
        var dictionary = [String:Any]()
        if token != nil{
            dictionary["token"] = token
        }
        if userId != nil{
            dictionary["user_id"] = userId
        }
        return dictionary
    }
    
    /**
     * NSCoding required initializer.
     * Fills the data from the passed decoder
     */
    @objc required init(coder aDecoder: NSCoder)
    {
        token = aDecoder.decodeObject(forKey: "token") as? String
        userId = aDecoder.decodeObject(forKey: "user_id") as? Int
    }
    
    /**
     * NSCoding required method.
     * Encodes mode properties into the decoder
     */
    func encode(with aCoder: NSCoder)
    {
        if token != nil{
            aCoder.encode(token, forKey: "token")
        }
        if userId != nil{
            aCoder.encode(userId, forKey: "user_id")
        }
        
    }
    
}


//profile Model

class PUserProfileModel : NSObject, NSCoding{
    
    var data : [PUserProfileData]!
    var error : Bool!
    var message : String!
    
    /**
     * Instantiate the instance using the passed json values to set the properties values
     */
    init(fromJson json: JSON!){
        if json.isEmpty{
            return
        }
        data = [PUserProfileData]()
        let dataArray = json["data"].arrayValue
        for dataJson in dataArray{
            let value = PUserProfileData(fromJson: dataJson)
            data.append(value)
        }
        error = json["error"].boolValue
        message = json["message"].stringValue
    }
    
    /**
     * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
     */
    func toDictionary() -> [String:Any]
    {
        var dictionary = [String:Any]()
        if data != nil{
            var dictionaryElements = [[String:Any]]()
            for dataElement in data {
                dictionaryElements.append(dataElement.toDictionary())
            }
            dictionary["data"] = dictionaryElements
        }
        if error != nil{
            dictionary["error"] = error
        }
        if message != nil{
            dictionary["message"] = message
        }
        return dictionary
    }
    
    /**
     * NSCoding required initializer.
     * Fills the data from the passed decoder
     */
    @objc required init(coder aDecoder: NSCoder)
    {
        data = aDecoder.decodeObject(forKey: "data") as? [PUserProfileData]
        error = aDecoder.decodeObject(forKey: "error") as? Bool
        message = aDecoder.decodeObject(forKey: "message") as? String
    }
    
    /**
     * NSCoding required method.
     * Encodes mode properties into the decoder
     */
    func encode(with aCoder: NSCoder)
    {
        if data != nil{
            aCoder.encode(data, forKey: "data")
        }
        if error != nil{
            aCoder.encode(error, forKey: "error")
        }
        if message != nil{
            aCoder.encode(message, forKey: "message")
        }
        
    }
    
}


class PUserProfileData : NSObject, NSCoding{
    
    var address : String!
    var binderCoverImage : String!
    var binderCoverTitle : String!
    var birthday : String!
    var city : String!
    var companyName : String!
    var country : String!
    var createdAt : String!
    var designation : String!
    var deviceData : String!
    var email : String!
    var firstName : String!
    var fullName : String!
    var gender : String!
    var id : Int!
    var image : String!
    var ipAddress : String!
    var isOnline : Int!
    var isOnlineTime : String!
    var isVerified : Int!
    var lastName : String!
    var latitude : String!
    var legalEntityEmail : String!
    var legalEntityName : String!
    var legalEntityPhone : String!
    var location : String!
    var loginAccessToken : String!
    var longitude : String!
    var parentId : String!
    var phone : String!
    var practiceId : String!
    var practiceName : String!
    var resetPasswordToken : String!
    var roleId : Int!
    var skillAndExperience : String!
    var skypeId : String!
    var state : String!
    var status : Int!
    var updatedAt : String!
    var userOtp : String!
    var verifyString : String!
    var zipcode : String!
    
    /**
     * Instantiate the instance using the passed json values to set the properties values
     */
    init(fromJson json: JSON!){
        if json.isEmpty{
            return
        }
        address = json["address"].stringValue
        binderCoverImage = json["binder_cover_image"].stringValue
        binderCoverTitle = json["binder_cover_title"].stringValue
        birthday = json["birthday"].stringValue
        city = json["city"].stringValue
        companyName = json["company_name"].stringValue
        country = json["country"].stringValue
        createdAt = json["created_at"].stringValue
        designation = json["designation"].stringValue
        deviceData = json["device_data"].stringValue
        email = json["email"].stringValue
        firstName = json["first_name"].stringValue
        fullName = json["full_name"].stringValue
        gender = json["gender"].stringValue
        id = json["id"].intValue
        image = json["image"].stringValue
        ipAddress = json["ip_address"].stringValue
        isOnline = json["is_online"].intValue
        isOnlineTime = json["is_online_time"].stringValue
        isVerified = json["is_verified"].intValue
        lastName = json["last_name"].stringValue
        latitude = json["latitude"].stringValue
        legalEntityEmail = json["legal_entity_email"].stringValue
        legalEntityName = json["legal_entity_name"].stringValue
        legalEntityPhone = json["legal_entity_phone"].stringValue
        location = json["location"].stringValue
        loginAccessToken = json["login_access_token"].stringValue
        longitude = json["longitude"].stringValue
        parentId = json["parent_id"].stringValue
        phone = json["phone"].stringValue
        practiceId = json["practice_id"].stringValue
        practiceName = json["practice_name"].stringValue
        resetPasswordToken = json["reset_password_token"].stringValue
        roleId = json["role_id"].intValue
        skillAndExperience = json["skill_and_experience"].stringValue
        skypeId = json["skype_id"].stringValue
        state = json["state"].stringValue
        status = json["status"].intValue
        updatedAt = json["updated_at"].stringValue
        userOtp = json["user_otp"].stringValue
        verifyString = json["verify_string"].stringValue
        zipcode = json["zipcode"].stringValue
    }
    
    /**
     * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
     */
    func toDictionary() -> [String:Any]
    {
        var dictionary = [String:Any]()
        if address != nil{
            dictionary["address"] = address
        }
        if binderCoverImage != nil{
            dictionary["binder_cover_image"] = binderCoverImage
        }
        if binderCoverTitle != nil{
            dictionary["binder_cover_title"] = binderCoverTitle
        }
        if birthday != nil{
            dictionary["birthday"] = birthday
        }
        if city != nil{
            dictionary["city"] = city
        }
        if companyName != nil{
            dictionary["company_name"] = companyName
        }
        if country != nil{
            dictionary["country"] = country
        }
        if createdAt != nil{
            dictionary["created_at"] = createdAt
        }
        if designation != nil{
            dictionary["designation"] = designation
        }
        if deviceData != nil{
            dictionary["device_data"] = deviceData
        }
        if email != nil{
            dictionary["email"] = email
        }
        if firstName != nil{
            dictionary["first_name"] = firstName
        }
        if fullName != nil{
            dictionary["full_name"] = fullName
        }
        if gender != nil{
            dictionary["gender"] = gender
        }
        if id != nil{
            dictionary["id"] = id
        }
        if image != nil{
            dictionary["image"] = image
        }
        if ipAddress != nil{
            dictionary["ip_address"] = ipAddress
        }
        if isOnline != nil{
            dictionary["is_online"] = isOnline
        }
        if isOnlineTime != nil{
            dictionary["is_online_time"] = isOnlineTime
        }
        if isVerified != nil{
            dictionary["is_verified"] = isVerified
        }
        if lastName != nil{
            dictionary["last_name"] = lastName
        }
        if latitude != nil{
            dictionary["latitude"] = latitude
        }
        if legalEntityEmail != nil{
            dictionary["legal_entity_email"] = legalEntityEmail
        }
        if legalEntityName != nil{
            dictionary["legal_entity_name"] = legalEntityName
        }
        if legalEntityPhone != nil{
            dictionary["legal_entity_phone"] = legalEntityPhone
        }
        if location != nil{
            dictionary["location"] = location
        }
        if loginAccessToken != nil{
            dictionary["login_access_token"] = loginAccessToken
        }
        if longitude != nil{
            dictionary["longitude"] = longitude
        }
        if parentId != nil{
            dictionary["parent_id"] = parentId
        }
        if phone != nil{
            dictionary["phone"] = phone
        }
        if practiceId != nil{
            dictionary["practice_id"] = practiceId
        }
        if practiceName != nil{
            dictionary["practice_name"] = practiceName
        }
        if resetPasswordToken != nil{
            dictionary["reset_password_token"] = resetPasswordToken
        }
        if roleId != nil{
            dictionary["role_id"] = roleId
        }
        if skillAndExperience != nil{
            dictionary["skill_and_experience"] = skillAndExperience
        }
        if skypeId != nil{
            dictionary["skype_id"] = skypeId
        }
        if state != nil{
            dictionary["state"] = state
        }
        if status != nil{
            dictionary["status"] = status
        }
        if updatedAt != nil{
            dictionary["updated_at"] = updatedAt
        }
        if userOtp != nil{
            dictionary["user_otp"] = userOtp
        }
        if verifyString != nil{
            dictionary["verify_string"] = verifyString
        }
        if zipcode != nil{
            dictionary["zipcode"] = zipcode
        }
        return dictionary
    }
    
    /**
     * NSCoding required initializer.
     * Fills the data from the passed decoder
     */
    @objc required init(coder aDecoder: NSCoder)
    {
        address = aDecoder.decodeObject(forKey: "address") as? String
        binderCoverImage = aDecoder.decodeObject(forKey: "binder_cover_image") as? String
        binderCoverTitle = aDecoder.decodeObject(forKey: "binder_cover_title") as? String
        birthday = aDecoder.decodeObject(forKey: "birthday") as? String
        city = aDecoder.decodeObject(forKey: "city") as? String
        companyName = aDecoder.decodeObject(forKey: "company_name") as? String
        country = aDecoder.decodeObject(forKey: "country") as? String
        createdAt = aDecoder.decodeObject(forKey: "created_at") as? String
        designation = aDecoder.decodeObject(forKey: "designation") as? String
        deviceData = aDecoder.decodeObject(forKey: "device_data") as? String
        email = aDecoder.decodeObject(forKey: "email") as? String
        firstName = aDecoder.decodeObject(forKey: "first_name") as? String
        fullName = aDecoder.decodeObject(forKey: "full_name") as? String
        gender = aDecoder.decodeObject(forKey: "gender") as? String
        id = aDecoder.decodeObject(forKey: "id") as? Int
        image = aDecoder.decodeObject(forKey: "image") as? String
        ipAddress = aDecoder.decodeObject(forKey: "ip_address") as? String
        isOnline = aDecoder.decodeObject(forKey: "is_online") as? Int
        isOnlineTime = aDecoder.decodeObject(forKey: "is_online_time") as? String
        isVerified = aDecoder.decodeObject(forKey: "is_verified") as? Int
        lastName = aDecoder.decodeObject(forKey: "last_name") as? String
        latitude = aDecoder.decodeObject(forKey: "latitude") as? String
        legalEntityEmail = aDecoder.decodeObject(forKey: "legal_entity_email") as? String
        legalEntityName = aDecoder.decodeObject(forKey: "legal_entity_name") as? String
        legalEntityPhone = aDecoder.decodeObject(forKey: "legal_entity_phone") as? String
        location = aDecoder.decodeObject(forKey: "location") as? String
        loginAccessToken = aDecoder.decodeObject(forKey: "login_access_token") as? String
        longitude = aDecoder.decodeObject(forKey: "longitude") as? String
        parentId = aDecoder.decodeObject(forKey: "parent_id") as? String
        phone = aDecoder.decodeObject(forKey: "phone") as? String
        practiceId = aDecoder.decodeObject(forKey: "practice_id") as? String
        practiceName = aDecoder.decodeObject(forKey: "practice_name") as? String
        resetPasswordToken = aDecoder.decodeObject(forKey: "reset_password_token") as? String
        roleId = aDecoder.decodeObject(forKey: "role_id") as? Int
        skillAndExperience = aDecoder.decodeObject(forKey: "skill_and_experience") as? String
        skypeId = aDecoder.decodeObject(forKey: "skype_id") as? String
        state = aDecoder.decodeObject(forKey: "state") as? String
        status = aDecoder.decodeObject(forKey: "status") as? Int
        updatedAt = aDecoder.decodeObject(forKey: "updated_at") as? String
        userOtp = aDecoder.decodeObject(forKey: "user_otp") as? String
        verifyString = aDecoder.decodeObject(forKey: "verify_string") as? String
        zipcode = aDecoder.decodeObject(forKey: "zipcode") as? String
    }
    
    /**
     * NSCoding required method.
     * Encodes mode properties into the decoder
     */
    func encode(with aCoder: NSCoder)
    {
        if address != nil{
            aCoder.encode(address, forKey: "address")
        }
        if binderCoverImage != nil{
            aCoder.encode(binderCoverImage, forKey: "binder_cover_image")
        }
        if binderCoverTitle != nil{
            aCoder.encode(binderCoverTitle, forKey: "binder_cover_title")
        }
        if birthday != nil{
            aCoder.encode(birthday, forKey: "birthday")
        }
        if city != nil{
            aCoder.encode(city, forKey: "city")
        }
        if companyName != nil{
            aCoder.encode(companyName, forKey: "company_name")
        }
        if country != nil{
            aCoder.encode(country, forKey: "country")
        }
        if createdAt != nil{
            aCoder.encode(createdAt, forKey: "created_at")
        }
        if designation != nil{
            aCoder.encode(designation, forKey: "designation")
        }
        if deviceData != nil{
            aCoder.encode(deviceData, forKey: "device_data")
        }
        if email != nil{
            aCoder.encode(email, forKey: "email")
        }
        if firstName != nil{
            aCoder.encode(firstName, forKey: "first_name")
        }
        if fullName != nil{
            aCoder.encode(fullName, forKey: "full_name")
        }
        if gender != nil{
            aCoder.encode(gender, forKey: "gender")
        }
        if id != nil{
            aCoder.encode(id, forKey: "id")
        }
        if image != nil{
            aCoder.encode(image, forKey: "image")
        }
        if ipAddress != nil{
            aCoder.encode(ipAddress, forKey: "ip_address")
        }
        if isOnline != nil{
            aCoder.encode(isOnline, forKey: "is_online")
        }
        if isOnlineTime != nil{
            aCoder.encode(isOnlineTime, forKey: "is_online_time")
        }
        if isVerified != nil{
            aCoder.encode(isVerified, forKey: "is_verified")
        }
        if lastName != nil{
            aCoder.encode(lastName, forKey: "last_name")
        }
        if latitude != nil{
            aCoder.encode(latitude, forKey: "latitude")
        }
        if legalEntityEmail != nil{
            aCoder.encode(legalEntityEmail, forKey: "legal_entity_email")
        }
        if legalEntityName != nil{
            aCoder.encode(legalEntityName, forKey: "legal_entity_name")
        }
        if legalEntityPhone != nil{
            aCoder.encode(legalEntityPhone, forKey: "legal_entity_phone")
        }
        if location != nil{
            aCoder.encode(location, forKey: "location")
        }
        if loginAccessToken != nil{
            aCoder.encode(loginAccessToken, forKey: "login_access_token")
        }
        if longitude != nil{
            aCoder.encode(longitude, forKey: "longitude")
        }
        if parentId != nil{
            aCoder.encode(parentId, forKey: "parent_id")
        }
        if phone != nil{
            aCoder.encode(phone, forKey: "phone")
        }
        if practiceId != nil{
            aCoder.encode(practiceId, forKey: "practice_id")
        }
        if practiceName != nil{
            aCoder.encode(practiceName, forKey: "practice_name")
        }
        if resetPasswordToken != nil{
            aCoder.encode(resetPasswordToken, forKey: "reset_password_token")
        }
        if roleId != nil{
            aCoder.encode(roleId, forKey: "role_id")
        }
        if skillAndExperience != nil{
            aCoder.encode(skillAndExperience, forKey: "skill_and_experience")
        }
        if skypeId != nil{
            aCoder.encode(skypeId, forKey: "skype_id")
        }
        if state != nil{
            aCoder.encode(state, forKey: "state")
        }
        if status != nil{
            aCoder.encode(status, forKey: "status")
        }
        if updatedAt != nil{
            aCoder.encode(updatedAt, forKey: "updated_at")
        }
        if userOtp != nil{
            aCoder.encode(userOtp, forKey: "user_otp")
        }
        if verifyString != nil{
            aCoder.encode(verifyString, forKey: "verify_string")
        }
        if zipcode != nil{
            aCoder.encode(zipcode, forKey: "zipcode")
        }
        
}
}
