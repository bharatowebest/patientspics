//
//  Modal.swift
//  PatientPics
//
//  Created by Raghu on 16/08/19.
//  Copyright © 2019 Om Prakash Jangid. All rights reserved.
//

import Foundation
import Alamofire_SwiftyJSON
import SwiftyJSON

//change password

class PChangePasswordModel : NSObject{
    
    //var data : AnyObject!
    var error : Bool!
    var message : String!
    
    /**
     * Instantiate the instance using the passed json values to set the properties values
     */
    init(fromJson json: JSON!){
//        if json.isEmpty{
//            return
//        }
//        data = json["data"]
        error = json["error"].boolValue
        message = json["message"].stringValue
}
}
