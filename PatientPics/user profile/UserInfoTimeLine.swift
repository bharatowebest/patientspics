//
//  UserInfoTimeLine.swift
//  PatientPics
//
//  Created by jai kalra on 14/08/19.
//  Copyright © 2019 Om Prakash Jangid. All rights reserved.
//

import UIKit

class UserInfoTimeLine: UIViewController {

    @IBOutlet weak var lblCreatedOn : UILabel!
    @IBOutlet weak var tblView : UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        NotificationCenter.default.addObserver(self, selector: #selector(updateTimeLine), name: Notification.Name("userProfile"), object: nil)
    }
    
    
  @objc  func updateTimeLine(){
        tblView.reloadData()
    }

}

extension UserInfoTimeLine : UITableViewDataSource,UITableViewDelegate{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        return UITableViewCell()
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        let view = Bundle.main.loadNibNamed("TimeLineFooter", owner: self, options: nil)?.first as! UIView
        let modal = unarchiveData(key: "userProfileDetail") as! PUserProfileModel
        let createdTime = (modal.data.first?.createdAt)!
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        let dt = formatter.date(from: createdTime)
        guard dt != nil else {
            return nil
        }
        formatter.dateFormat = "EEEE, MMM dd, yyyy"
        let strDt = formatter.string(from: dt!)
        lblCreatedOn.text = strDt
        return view
    }

    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 100
    }
    
    
}
