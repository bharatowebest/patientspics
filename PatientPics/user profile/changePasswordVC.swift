//
//  changePasswordVC.swift
//  PatientPics
//
//  Created by jai kalra on 14/08/19.
//  Copyright © 2019 Om Prakash Jangid. All rights reserved.
//

import UIKit

class changePasswordVC: UIViewController {

    @IBOutlet weak var txtFieldCurrentPassword : UITextField!
     @IBOutlet weak var txtFieldNewPassword : UITextField!
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    func changePassword(){
        guard txtFieldCurrentPassword.text?.trimmingCharacters(in: .whitespaces) != "" else {
            self.showToast(message: "Please enter current password")
            return
        }
        
        guard txtFieldNewPassword.text?.trimmingCharacters(in: .whitespaces) != "" else {
            self.showToast(message: "Please enter new password")
            return
        }
        
        guard Reachability.isConnectedToNetwork() else {
            self.showToast(message: MessageString.INTERNET_CONNECTION_MESSAGE)
            return
        }
        
        self.showLoader()
        let param = ["current_password":txtFieldCurrentPassword.text! , "new_password" : txtFieldNewPassword.text!]
        postRequest(strUrl: "change-password", param: param as [String : Any], success: { (json) in
            self.hideLoader()
            let modal = PChangePasswordModel(fromJson: json)
            if !modal.error{
                self.txtFieldCurrentPassword.text = ""
                self.txtFieldNewPassword.text = ""
            }
             self.showToast(message: modal.message)
        }) { (err) in
            self.hideLoader()
        }
    }

    
    @IBAction func submitBtnTapped(_ sender : UIButton){
        changePassword()
    }

}
