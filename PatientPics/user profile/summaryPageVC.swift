//
//  summaryPageVC.swift
//  PatientPics
//
//  Created by jai kalra on 14/08/19.
//  Copyright © 2019 Om Prakash Jangid. All rights reserved.
//

import UIKit

class summaryPageVC: UIViewController {

    @IBOutlet weak var txtFieldFirstName : UITextField!
    @IBOutlet weak var txtFieldLastName : UITextField!
    @IBOutlet weak var txtFieldDOB : UITextField!
    @IBOutlet weak var txtFieldEmail : UITextField!
    @IBOutlet weak var txtFieldGender : UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
         NotificationCenter.default.addObserver(self, selector: #selector(updateUser), name: Notification.Name("userProfile"), object: nil)
    }
    
   @objc func updateUser(){
        let modal = unarchiveData(key: "userProfileDetail") as! PUserProfileModel
        txtFieldFirstName.text = modal.data.first?.firstName
         txtFieldLastName.text = modal.data.first?.lastName
         txtFieldDOB.text = modal.data.first?.birthday
         txtFieldEmail.text = modal.data.first?.email
         txtFieldGender.text = modal.data.first?.gender
    }


  
}
