//
//  userProfileRoot.swift
//  PatientPics
//
//  Created by jai kalra on 14/08/19.
//  Copyright © 2019 Om Prakash Jangid. All rights reserved.
//

import UIKit
import CarbonKit


class userProfileRoot: UIViewController , CarbonTabSwipeNavigationDelegate {

   
    @IBOutlet weak var viewContainer : UIView!
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        let items = ["Summary", "Timeline", "Change Password" , "Contact Info"]
        let carbonTabSwipeNavigation = CarbonTabSwipeNavigation(items: items, delegate: self)
        carbonTabSwipeNavigation.insert(intoRootViewController: self, andTargetView: viewContainer)
        carbonTabSwipeNavigation.setNormalColor(UIColor.darkGray,font: UIFont.boldSystemFont(ofSize: 16))
        
        carbonTabSwipeNavigation.setSelectedColor( APP_THEME_COLOR, font: UIFont.boldSystemFont(ofSize: 16))
        carbonTabSwipeNavigation.setIndicatorColor( APP_THEME_COLOR)
        
        self.navigationItem.leftBarButtonItem = nil
        self.navigationController?.navigationBar.isHidden = false
        self.title = "Profile"
        let backBarButtonItem = UIBarButtonItem(image: UIImage(named: "menu-button-2.png"), style: .plain, target: self, action: #selector(backBtn))
        self.navigationController?.navigationBar.tintColor = .white
        self.navigationItem.leftBarButtonItem  = backBarButtonItem
        
        
    }
    
    @objc func backBtn()
    {
        sideMenuController?.showLeftViewAnimated()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        let dispatchGroup = DispatchGroup()
        
        dispatchGroup.enter()
        getProfileDetail {
            dispatchGroup.leave()
        }
        
        dispatchGroup.notify(queue: .main){
          NotificationCenter.default.post(name: Notification.Name("userProfile"), object: nil)
        }
    }
    
    
    func carbonTabSwipeNavigation(_ carbonTabSwipeNavigation: CarbonTabSwipeNavigation, viewControllerAt index: UInt) -> UIViewController {
        switch index {
        case 0:
            let vc = summaryPageVC(nibName: "summaryPageVC", bundle: nil)
           
            return vc
        case 1:
            let vc = UserInfoTimeLine(nibName: "UserInfoTimeLine", bundle: nil)
          
            return vc
        case 2:
            let vc = changePasswordVC(nibName: "changePasswordVC", bundle: nil)
            return vc
        default:
            let vc = ContactInfoVC(nibName: "ContactInfoVC", bundle: nil)
            return vc
        }
    }
    
    func getProfileDetail(completionHandler : @escaping()->Void){
      
        var parameter = [String:Any]()
      
        showLoader()
        postRequest(strUrl: "view-profile", param: [:], success: { (json) in
            print(json)
            self.hideLoader()
            
           let model = PUserProfileModel(fromJson: json)
            let data = try? NSKeyedArchiver.archivedData(withRootObject: model, requiringSecureCoding: false)
            UserDefaults.standard.setValue(data, forKey: "userProfileDetail")
           completionHandler()
            
        }) { (msg) in
            self.ShowAlert(message: msg)
             self.hideLoader()
        }
        
}


}
